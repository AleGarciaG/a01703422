<?php
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaDiagnostico/_consultaDiagnosticoTitulo.html");

    if (isset($_POST["tratamiento"])){
        print "
        <div class='alert alert-success alert-dismissible fade show'>
            <button type='button' class='close' data-dismiss='alert'>&times;</button>
            <strong>Operacion exitosa!</strong> Se agrego un nuevo diagnostico.
        </div>
        ";
    }

        echo "<div class=\"row\">";
            echo "<div class=\"col-4\">";
            include("Partials/ConsultaDiagnostico/_consultaDiagnostico.html");
            echo "</div>";
            if(isset($_POST['diagnostico'])){
                echo "<div class=\"col-8\">";
                include("Partials/ConsultaDiagnostico/_diagnostico.html");
                echo "</div>";
            }
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>