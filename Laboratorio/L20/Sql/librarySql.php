<?php
    function conectar_bd() {

        $conexion_bd = mysqli_connect("localhost","root","","needed");

        if ($conexion_bd == NULL) {

            die("No se pudo conectar con la base de datos");

        }

        return $conexion_bd;
    }

//función para desconectarse de una bd

    function desconectar_bd($conexion_bd) {

        mysqli_close($conexion_bd);

    }

    function cleamData ($dataToCleam){
        return stripslashes(trim(htmlspecialchars($dataToCleam)));
    }

    function getProgramas(){
        $conn = conectar_bd();
        $sql = "SELECT P.idProgramaAtencion as P_id,  A.nombre as A_nombre, P.nombreProgramaAtencion as P_nombre,DATE_FORMAT(P.fechaInicial, '%d/%m/%Y') as P_fechaInicio FROM ProgramaAtencion as P, Area as A WHERE P.idArea = A.idArea";
        $result = mysqli_query($conn, $sql);
        $table = "
        <form id='programa'>
        <table class='table'>
        <thead>
            <tr>
            <th>Ver</th>
            <th>Area</th>
            <th>Nombre</th>
            <th>Fecha Inicio</th>
            </tr>
        </thead>
        <tbody id='myTable'>
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $table.= "<tr>";
                $table.= "<td><button  type='button' name='receta' class='btn' onclick='mostrarPrograma(". $row["P_id"]. ")'/>Programa</button></td> ";
                $table.= "<td>". $row["A_nombre"]. "</td>";
                $table.= "<td>". $row["P_nombre"]. "</td>";
                $table.= "<td>". $row["P_fechaInicio"]. "</td>";
                $table.= "</tr>";
            }
            $table.= "        
                </tbody>
            </table>
        </form>";
        desconectar_bd($conn);
        return $table;
    }
    function getPrograma($idPrograma){
        $conn = conectar_bd();
        $sql = "SELECT A.nombre as A_nombre,P.objetivo as P_objetivo ,P.nombreProgramaAtencion as P_nombre,DATE_FORMAT(P.fechaInicial, '%d/%m/%Y') as P_fechaInicio,DATE_FORMAT(P.fechaFinal, '%d/%m/%Y') as P_fechaFinal FROM ProgramaAtencion as P, Area as A WHERE P.idArea = A.idArea  and $idPrograma=P.idProgramaAtencion";
        $result = mysqli_query($conn, $sql);
        $resultado = "
        <div class='card blue-grey darken-1'>

        <div class='card-content white-text'>
     
           <span class='card-title'>Programa:
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $resultado.= $row["P_nombre"]. "<br>Fecha de inicio:";
                $resultado.= $row["P_fechaInicio"]. "</span>
                <p>Descripción:
                ";
                $resultado.= $row["P_objetivo"]. "</p>
                <p>Fecha final de programa:
                ";
                $resultado.= $row["P_fechaFinal"]."</p>
                    </div>
                    <div class='card-action'>

                    <a href='#'>Consultar Programa</a>
            
                </div>
            
                </div>
                
                ";
            }
        desconectar_bd($conn);
        return $resultado;
    }
    function getRecetas(){
        $conn = conectar_bd();
        $sql = "SELECT M.nombre as M_nombre, R.idReceta as R_id,  DATE_FORMAT(R.fechaIni, '%d/%m/%Y') as R_fechaInicio, B.apellidoM as B_apellidoM, B.apellidoP as B_apellidoP,B.nombre as B_nombre FROM Receta as R, Beneficiaria as B,Medicamentos as M WHERE R.idDeIngreso = B.idDeIngreso and R.idMedicamento = M.idMedicamento ";
        $result = mysqli_query($conn, $sql);
        $table = "
        <form id='programa'>
        <table class='table'>
        <thead>
            <tr>
            <th>Ver</th>
            <th>Beneficiaria</th>
            <th>Medicamento</th>
            <th>Fecha Inicio</th>
            </tr>
        </thead>
        <tbody id='myTable'>
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $table.= "<tr>";
                $table.= "<td><button  type='button' name='receta' class='btn' onclick='mostrarReceta(". $row["R_id"]. ")'/>Receta</button></td> ";
                $table.= "<td>". $row["B_nombre"].' '.$row["B_apellidoP"].' '.$row["B_apellidoM"]. "</td>";
                $table.= "<td>". $row["R_fechaInicio"]. "</td>";
                $table.= "<td>". $row["M_nombre"]. "</td>";
                $table.= "</tr>";
            }
            $table.= "        
                </tbody>
            </table>
        </form>";
        desconectar_bd($conn);
        return $table;
    }
    function getReceta($idReceta){
        $conn = conectar_bd();
        $sql = "SELECT M.nombre as M_nombre,R.dosis as R_dosis, R.descripcion as R_descripcion,DATE_FORMAT(R.fechaFin, '%d/%m/%Y') as R_fechaFin,DATE_FORMAT(R.fechaIni, '%d/%m/%Y') as R_fechaInicio, B.apellidoM as B_apellidoM, B.apellidoP as B_apellidoP,B.nombre as B_nombre FROM Receta as R, Beneficiaria as B,Medicamentos as M WHERE R.idDeIngreso = B.idDeIngreso and R.idMedicamento = M.idMedicamento and $idReceta=R.idReceta ";
        $result = mysqli_query($conn, $sql);

        $resultado = "
        <div class='card blue-grey darken-1'>

        <div class='card-content white-text'>
     
           <span class='card-title'>Paciente:
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $resultado.= $row["B_nombre"].' '.$row["B_apellidoP"].' '.$row["B_apellidoM"]."<br>Fecha de inicio:";
                $resultado.= $row["R_fechaInicio"]. "</span>
                <p>Indicaciones:
                ";
                $resultado.= $row["R_descripcion"]. "</p>
                <p>Medicamento:
                ";
                $resultado.= $row["M_nombre"]. "</p>
                Dosis:
                ";
                $resultado.= $row["R_dosis"]
                ;
                $resultado.="<br>Fecha final de tratamiento" .$row["R_fechaFin"]."</p>
                    </div>
                    <div class='card-action'>

                    <a href='#'>CONSULTAR EXPEDIENTE</a>
            
                </div>
            
                </div>
                
                ";
            }
        desconectar_bd($conn);
        return $resultado;
    }


/*
    CREATE TABLE `Receta`
    (
        `idReceta` int(11) not null,
        `idDeIngreso` int(11) not null,
        `idMedicamento` int(11) not null,
        `fechaIni` DATETIME not null,
        `fechaFin` DATETIME,
        `descripcion` varchar(300) not null,
        `dosis` varchar(50) not null
    );
    CREA
    CREATE TABLE `Beneficiaria`
(
	`idDeIngreso` int(11) not null ,
  `idTipoSangre` int(11) ,
  `idCiudad` int(11) ,
	`fechaHoraIngreso` timestamp,
	`nombre` varchar(40),
	`apellidoM` varchar(40),
	`apellidoP` varchar(40),
	`fechaNacimiento` DATETIME ,
	`curp` varchar(18),
	`noDeExpediente` varchar(50),
	`ingresoConHermanos` BOOLEAN,
	`motivoIngreso` varchar(5000),
	`noDisposicion` int(11),
	`consideracionesGenerales` varchar(5000)

);
CREATE TABLE `Medicamentos`
(
	`idMedicamento` int(11) not null,
	`nombre` varchar(40) not null,
	`ingredienteActivo` varchar(40) not null,
    `idPresentacion` int(11) not null
);
*/
?>