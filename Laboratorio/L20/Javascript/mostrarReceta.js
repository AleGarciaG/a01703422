async function mostrarReceta(idReceta) {
  //Para poder pasar parámetros
  let parametros = new FormData();
  parametros.append("Recetaid", idReceta);
  try {
    //await sirve para indicar que en este punto se espera una petición asíncrona
    //fetch es la función que hace la petición asíncrona
    await fetch('controladorBuscarReceta.php', {
      method: 'POST',
      body: parametros
      //.then se ejecuta cuando concluye la petición asíncrona, pero esto también genera una promesa que se ejecuta de manera asíncrona
    }).then(function (response) {
      return response.text();
      //Este segundo then nos permite recuperar el contenido de la respuesta cuando se termina la promesa anterior
    }).then(function (data) {
      document.getElementById("resultadosConsultaRecetas").innerHTML = data;
    });
    //El uso de async y await permite que atrapar un error en la comunicación
  }
  catch (e) {
    console.log(e);
    document.getElementById("resultadosConsultaRecetas").innerHTML = "Error en la comunicación con el servidor";
  }
}
