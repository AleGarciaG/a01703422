
//Función que detonará la petición asíncrona como se hace ahora con js vanilla
//async sirve para indicar que la función hace una petición asíncrona
async function mostrarPrograma(idPrograma) {
  //Para poder pasar parámetros
  let parametros = new FormData();
  parametros.append("Programaid", idPrograma);
  try {
      //await sirve para indicar que en este punto se espera una petición asíncrona
      //fetch es la función que hace la petición asíncrona
      await fetch('controladorBuscarProgramas.php', {
          method: 'POST', //El método GET de fetch no permite parámetros
          body: parametros
              //.then se ejecuta cuando concluye la petición asíncrona, pero esto también genera una promesa que se ejecuta de manera asíncrona
      }).then(function (response) {
          return response.text();
          //Este segundo then nos permite recuperar el contenido de la respuesta cuando se termina la promesa anterior
      }).then(function (data) { //
          document.getElementById("resultadosConsultaProgramas").innerHTML = data;
      });
      //El uso de async y await permite que atrapar un error en la comunicación
  } catch (e) {
      console.log(e);
      document.getElementById("resultadosConsultaProgramas").innerHTML = "Error en la comunicación con el servidor";
  }
}

