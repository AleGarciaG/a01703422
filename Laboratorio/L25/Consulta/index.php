<?php 
require '../flight/Flight.php';
Flight::route('', function(){
    session_start();
    if(isset($_SESSION['pdid'])){
      delate_caso_pro($_SESSION['pdid']);
      session_unset();
    }
    require_once("../util.php");  
    include("../Partials/_header.html");  
    include("../Partials/_navBar.html");
    if (isset($_POST["EliminarProductoid"])){
      if (delate_caso_pro($_POST["EliminarProductoid"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se elimino un producto.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al eliminar un producto.
          </div>
          ";
      }
  }
    
    if (isset($_POST["newPais"])){
        if (insertar_caso_p($_POST["newPais"])){
            print "
            <div class='alert alert-success alert-dismissible fade show'>
                <button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Success!</strong> Se agrego un nuevo pais.
            </div>
            ";
        }else{
            print "
            <div class='alert alert-danger alert-dismissible fade show'>
                <button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Error!</strong> Error al agregar un nuevo pais.
            </div>
            ";
        }
    }
    if (isset($_POST["newFruta"])){
      if (insertar_caso_f($_POST["newFruta"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se agrego una nueva Fruta.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al agregar una nueva fruta.
          </div>
          ";
      }
  }
  if (isset($_POST["FrutaSEliminar"])){
      if (delate_caso_f($_POST["FrutaSEliminar"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se eilimino una  Fruta.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al eliminar una  fruta.
          </div>
          ";
      }
  }
  if (isset($_POST["PaisesSEliminar"])){
      if (delate_caso_p($_POST["PaisesSEliminar"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se eilimino un pais.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al eliminar un  pais.
          </div>
          ";
      }
  }
  
  if ((isset($_POST["FrutaSProducto"])) and (isset($_POST["PaisesSProducto"])) and (isset($_POST["newUnidades"])) and (isset($_POST["newCantidad"])) and (isset($_POST["newPrecio"]))){
      if (insertar_caso_pro($_POST["FrutaSProducto"],$_POST["newUnidades"],$_POST["newCantidad"],$_POST["newPrecio"],$_POST["PaisesSProducto"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se agrego un nuevo producto
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al agregar un nuevo producto/
          </div>
          ";
      }
  }
  if ((isset($_POST["ModificarProductoid"]))  and (isset($_POST["FrutaSModificarProducto"]))  and (isset($_POST["ModificarProductoUnidades"])) and (isset($_POST["ModificarProductoCantidad"])) and (isset($_POST["ModificarProductoPrecio"]))and (isset($_POST["PaisesSModificarProducto"]))){
      if (modificar_caso_pro($_POST["ModificarProductoid"],$_POST["FrutaSModificarProducto"],$_POST["ModificarProductoUnidades"],$_POST["ModificarProductoCantidad"],$_POST["ModificarProductoPrecio"],$_POST["PaisesSModificarProducto"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se modifico un producto
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al modificar un producto/
          </div>
          ";
      }
  }
  
  if ((isset($_POST["modificarFruta"])) and (isset($_POST["FrutaSModificar"])) ){
      if (modificar_caso_f($_POST["FrutaSModificar"],$_POST["modificarFruta"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se modifica una Fruta.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al modificar una fruta.
          </div>
          ";
      }
  }
  if ((isset($_POST["modificarPais"])) and (isset($_POST["PaisesSModificar"])) ){
      if (modificar_caso_p($_POST["PaisesSModificar"],$_POST["modificarPais"])){
          print "
          <div class='alert alert-success alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Success!</strong> Se modifica una Fruta.
          </div>
          ";
      }else{
          print "
          <div class='alert alert-danger alert-dismissible fade show'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              <strong>Error!</strong> Error al modificar una fruta.
          </div>
          ";
      }
  }
  
    if (isset($_POST["FrutaG"])) {
      $fruta = htmlspecialchars($_POST["FrutaG"]);
      } else {
          $fruta = "";
      }
  
      if (isset($_POST["PaisesG"])) {
          $paises = htmlspecialchars($_POST["PaisesG"]);
      } else {
          $paises = "";
      }
      if (isset($_POST["ordenarBy"])) {
          $ordenarBy = htmlspecialchars($_POST["ordenarBy"]);
      } else {
          $ordenarBy = "";
      }
    include("../Partials/_consultaForm.html"); 
    viewQuery(getFruits($paises,$fruta,$ordenarBy));
  
    include("../Partials/_footer.html"); 
    include("../Partials/_endPage.html"); 
  
  
    
  
  
      function add($a,$b){
          $c=$a+$b;
          return $c;
        }
});
Flight::start();
  
?> 
