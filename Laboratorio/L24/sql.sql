DROP TABLE IF EXISTS  Clientes_Banca;
DROP TABLE IF EXISTS  Tipos_Movimiento;
DROP TABLE IF EXISTS  Movimientos ;
CREATE TABLE `Clientes_Banca`
(
	`NoCuenta` varchar(5) not null ,
	`Nombre`   varchar(30),
	`Saldo` numeric(10,2),
	PRIMARY KEY (`NoCuenta`)
);
CREATE TABLE `Tipos_Movimiento`
(
	`ClaveM` varchar(2) not null ,
	`Descripcion` varchar(30),
  PRIMARY KEY (`ClaveM`)
);
CREATE TABLE `Movimientos`
(
	`Fecha` datetime not null,
	`Monto` numeric(10,2)
);