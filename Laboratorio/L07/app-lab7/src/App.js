import React from 'react';
import './App.css';
import Bienvenida from  './Componentes/Bienvenida';
import Descripcion from './Componentes/Descripcion';
import PreguntasL1 from './Componentes/PreguntasL1';
import Misinteres from './Componentes/MisIntereses';
import PieDePagina from './Componentes/PieDePagina';


function App() {
  return (
    <React.Fragment>
      <Bienvenida/>
      <Misinteres/>
      <Descripcion/>
      <PreguntasL1/>
      <PieDePagina/>
    </React.Fragment>
  );
}


export default App;
