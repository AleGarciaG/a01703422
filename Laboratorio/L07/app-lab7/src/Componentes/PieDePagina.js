import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

export default function SimpleExpansionPanel() {
  return (
    <Grid>
      <Grid>
        <Typography gutterBottom variant="h5" component="h2" textAlign="center">
          <Box textAlign="center" m={1}>
            Referencia
          </Box>
        </Typography>
      </Grid>
      <Grid container spacing={3}>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              Berners-Lee, Tim (2004) Architecture of the World Wide Web, Volume
              One. Recuperado de: https://www.w3.org/TR/webarch/#acks
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              Fielding, et al.(1999) Hypertext Transfer Protocol -- HTTP/1.1.
              Recuperado de: https://www.w3.org/Protocols/rfc2616/rfc2616.html
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              MDN (s.f) Sending form data. Recuperado de:
              https://developer.mozilla.org/es/docs/Learn/HTML/Forms/Sending_and_retrieving_form_data
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              MDN (s.f) Códigos de Estado de Respuesta. Recuperado de:
              https://developer.mozilla.org/es/docs/Web/HTTP/Status
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              W3C (s.f). Conformance: requirements and recommendations.
              Recuperado de: https://www.w3.org/TR/REC-html40/conform.html
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              W3C (2014). HTML5 Differences from HTML4. Recuperado de:
              https://www.w3.org/TR/html5-diff/#obsolete-attributes
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={1}>
              MDN (s.f) Conceptos básicos de las tablas HTML. Recuperado de:
              https://developer.mozilla.org/es/docs/Learn/HTML/Tablas/Conceptos_b%C3%A1sicos_de_las_tablas_HTML
            </Box>
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant="h15" textAlign="center">
            <Box textAlign="center" m={3}>
              Berzal, F. (s.f.) El ciclo de vida de un sistema de información.
              Recuperado de:
              http://flanagan.ugr.es/docencia/2005-2006/2/apuntes/ciclovida.pdf
            </Box>
          </Typography>
        </div>
      </Grid>
      










      <Grid container spacing={3}>
      <div>
        <Typography gutterBottom variant="h5" textAlign="center">
          <Box textAlign="center" m={3}>
          Alexis Garcia Gutierrez
          </Box>
        </Typography>
      </div>
      </Grid>

      <Grid container spacing={3}>

      <div>
        <Typography gutterBottom variant="h15" textAlign="center">
          <Box textAlign="center" m={3}>
          Matricula A01703422
          </Box>
        </Typography>
      </div>
      <div>
        <Typography gutterBottom variant="h15" textAlign="center">
          <Box textAlign="center" m={3}>
          Correo Institucional a01703422@itesm 
          </Box>
        </Typography>
      </div>
      <div>
        <Typography gutterBottom variant="h15" textAlign="center">
          <Box textAlign="center" m={3}>
          Herramienta utilizada visual code
          </Box>
        </Typography>
      </div>
    
    </Grid>
    </Grid>
  );
}
