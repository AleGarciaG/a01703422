import React from "react";
import { Container } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

export default function Home() {
  const useStyles = makeStyles(theme => ({
    root: {
      maxWidth: 345
    },
    media: {
      height: 140
    }
  }));
  const classes = useStyles();
  return (
    <Container>
      <Grid>
        <Grid>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            textAlign="center"
          >
            <Box textAlign="center" m={1}>
              Breve descripción sobre
            </Box>
          </Typography>
        </Grid>

        <Grid container spacing={3}>
          <div>
            <br></br>

            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://sitechecker.pro/wp-content/uploads/2017/12/URL-meaning.png"
                  title="URL"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    URL(Uniform Resource Locator)
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Es la secuencia de caracteres que identifica y permite
                    localizar y recuperar una información determinada en
                    Internet.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>

          <div>
            <br></br>

            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUSEhAVFRUVFxYVFhUVFRgWFRUVFhUXFh0VFRUYICgiGB0lHRYWITEhJykrLi4uFx8zODgtNyktLisBCgoKDg0OGxAQGzcmICIuLS0yLTAvMjItNTctLS03Ky0uLjU2LS8yKy4yLS81LzU3MjQ2Li0wLTYvOC0uLy4tLf/AABEIAMoA+QMBEQACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAQIFAwQGB//EAE4QAAIBAwIDAwcIBgYHCAMAAAECAwAEEQUSBiExE0FRFCIyUmFxgQcXI3KRk6HhFTM0QmKxFkOCg6LBREVjc8LR0jVVkpSjpLKzJCVU/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAMEAQIFBv/EADsRAQACAQICBQsCBAUFAAAAAAABAgMEESExBRITQVEiMmFxgZGhscHR8FLhFCNCQzOCkqLxFSVEYnL/2gAMAwEAAhEDEQA/APaaAoCgKAoIsmaCVAYoJCgMUDoHQGKA20DxQGKAxQLFAYoCgiwzQILigeKCOKAoCgKAoFigdAUBQFAUBQFAqB0BigkKB0DoDFA6B4oCgKAoCgKAoFQGKBUCoFQIigVAUBQFAUCoHQFAUBQFAUDFA6B0DxQOgRoGBQSxQGKB4oFigKAxQLFAUBQKgRFAqBUEWoEBQOgKAoCgWKB0CNAUDoAUEqB0DAoHQRoMgFA6AoCgKAoEaAFA6Cv1bXLS2ANxcRRZ6B3ClvYqnmx91BWf0p7T9msbufwbsvJ4/fuuSmR7VBoMTz61IfMgsrceMsstw/8A4I1jH+I/GgzJYaqfTv7cDwjsmB+DPO38qBvot4euqzr9SC1A/wAcTH8aCA0O6z/2tdnp1js8e7lB0oIPot+PQ1aTOP622t3GfE7FQ/YRQa8kGuR+hPYTjvEkM1ux9zI7gfZQaFzxvc2hzqOmSwxZx5RA4uYQPWfaAyD3jNB1mm6jDcRrNBKskbei6HIPiPYR3g8xQbVAUBQFAUBQFBIUDFAxQM0CFBMCgKB0BQFAUBQFBRcXcVW2nRCScks52xRIN0sr+qi/Ec+gyPEZDkdTvdQeA3eqXR0u0yo7C2Be5wxAXtZwpKEnuRR154IoOk0zTtLsZ4oYolE9wJGVyDJM4QBmZ5ny2OY6nv5UF5q+opbQS3EmdkSNI2OuEUtge3lyoNK/1CTyi1hiwO07SWXcMkQRIAQPBjJLCOfdu8KDatdRRyEYiOYqW7BnQyhQSN21SeXLr7aCH6WtysbiaPZLns23ja+EZztPQjajH3A0FQeNbHDsrSyKh2747eV45H3hOzhkVdsr7j0UnoT0BIBXfGdsBCIo5riSYOUhhj+lAibbIZBIVEe1vNIYg55UGC042hmu47WGMsHSOTtWkSLzJYu1RoonIeYY2glR5pb2Gg1dI44F5drBBAHt27QGXfl9qK30piAISJmGxS7AtuyBgGg5PRrN7HiRrSyO21mjM80I/Vx5jY8h+75+zGOgcDpQevUBQLNA6AoCgYFAxQSoHQGKCQFA6AoFQOgKAoCgKDy5oRPxURccxb2gktlJ5b8plgO8+fIf7IPdQWHyj6c+pyppcbbQsMl3K3MANtaK3RiO4yFmI8IqCk13SL/tWmkP/wCdNY3/AGa27PiFImthFDC3IlvPkJbGSz4HICgurnyq/kUtFNHb3E0EYjkVkIgtd9zJLIh9HtJQkQB5lRnvoD+i932l7PJcXFxcxxbbQl+whZjEzABIyqEdqSMNnAAz40Fdwxot1M4nS2gCPdRzpfNJunNtAI4VSJQmQJI4T1fG2ZvGg37Lga8NrJb3FxE3Z2txaWu3eRifIM85Izv2hFwMgANzO6g3db0xnls7CzkWDyRfKS3Z9oqKitBErJuXO4vIev8AVE0GT+g2SjvfXBkIlWeRdiNcJMYy0eQv0K/RIBswQM8+eaCy1LQO0eMm5eO2i7NvJlWIR7oWDIe0K70AKryDD0R0oOZ0yO1t3ZdJWe5bzwsazMNOgLtli8noEg/uje4xgAUF5wnwsLVpriaTtru5IaebGFwOkUS/uovIeJwM9AAHR0BQFAUBQFBIUDoCgkBQSFA6AoCgKAoIq3dQM9OX40HkPEnGmrWas1vcQagqvskfyRo4kdjtESSLNiV8kDYm4jvxQemcNy3b20T3iIlwy7pEjBCISSQuCWOQMA8zzBoKXjLgdL2SK6ine2vIOUVxHz5ZJ2uh9Jebcsj0iOYOKCFpdazAfp7GC7OApmtZVilZVJxuimwM+cxwHxzOOtBvrxSAMy2F7EfDyczH/wBuZKDIvFdsTjZdA/xWN4v4mLFBj1DX7SSJ42NwFkRkJS2ug4DKQSpWPIPPkaA07iC0VEihhugiKEQCxu0UKgCgAtEB0AHWg221eQ+hY3LDx+hQfESSqfwoMQN2XLpZwIzhVZ5Jz2hVdxUERxkMBubA3fvGgbWV8+d94kYPTyeAB1/tzNIrf+AUGM8MWzHM4e5PL9pdpUyOhEJ+iU+0IKC3VQAAAAByAHIAeAFBBmxQSoCgKAoCgYoGKB0EhQOglQFAUBQFAqCg1PiyBJDBAr3VyOsFuAxQ/wC2kJCQj67D2A0GH9CXF3z1CQCM/wChwMwiI8J5uTT/AFcKh6ENQZ7jhaF7i3mPKK0U9hbKoWJJDy7UqORIXAUY83me/kF/QFAUFdquvWdt+0XUMPgJJFUn3AnJ+FBTrx5Zt+pS6uPbBaTuh9z7ApHtzigzDiaZvQ0m+b2kW8Y/9SYEfZQTGtXx6aVKPr3FuP8A4O1Azquo/wDdg/8ANR/9NBgbXr9fS0ecjxjuLZvwaRTQYX42VP1+nahCO9jbGRPfugZxQZNM480q4bZHfRbyduyQmJ93TbtlCnPsoOiNAjQRoHQFAUBQMUADQT6UHN6px1YwtsV2nk6COAdoc+G4eb8M5qG2opHCOM+h1NP0PqcsdeY6tfG3D92lBxPqkvOLR2A7jLMEOPEqyitYy5J5U+Kxbo/Q4/P1Meyu/wAplti91o9LO1X2NMx/lWetm8I96HsujY/uWn/LBNqGtKMmxt39iT7T/i5U62X9Me8jD0db+7aPXX7MLcZ3EP7XpdxGPWjImUDxJGAKx21o86s/NJHRWLL/AIGes+ifJn6rbR+LLG6wIrhSx/cbzH+CtjPwzUlM1L8pU9T0bqtPxyUnbx5x74XVSKKl1vQXunAku5kt9uGghPZdocnPaTL5+3GBtUr055oLDTNMgtoxFBCkUY6KihR7+XU+3rQbdAqCs4g4itLGPtbqdYl7s82Y+CIObH3Cg56DiLVL3nZWIt4T0uL7KswzzKWqeceXMFmAOaDPJw7naL/U55mc7RGsgtIWPqrHCVZ/cztQXWlcOWVtzgtIY2PVkjUOfe+Mn4mgy3OrRpcQ2xDGSZZHG0ZCpEF3O5/dGWVR4lh7aDfoCgVAGgoeI7yXtbS2hfY002+RgASLeAdpJgH1j2cee7taDPr3DtnersubaOUYxll89fqyDzlPuNB5fq1/dcNTxASPc6ZMSFjkOZbcrjKRueXIcwOQOCOR84h6+jhlDDoQCPcRmgdAqAoHQKgYoKHibiyGzxGFM07+hCnNiT03YztB+JPcKiyZYpw5z4OloejMmq3vv1aRztPL2K6Dhy8vfP1KYpGeYtITtQDwlYekfZk+8dKj7K2Tjkn2LVtdp9J5OjrvP67cZ9kd35wdTpmlW9uu2CFIx37VAJ+serH2mp60rXhEOTm1GXPbrZbTM+luitkJ0BQLNBQ69wdZXYJkhCuf6yPCvnxJHJviDUV8NL84dDSdKanTeZbh4Txj9vY5UXd/o8ii4drmyYhRIcl4s+PePdkg45YPKod74Z48YdbstL0pSZwx1Mscdu6fz3+O/N6LBMrqrowZWAZWHMEEZBBq1ExMbw83atqWmto2mGSstRQVXFWtLZWk90wz2SFgucbm6Kue7LEDPtoOH+TPhlrgLrGo/TXU/nwhxlIIckp2adASPOHgCO8kkOv4rtp3QGO9NrFHved0RTIUVc4VmyEGA2TjPT4hxvClnYW2nrq1+jMxJmSW4LTzJG8n0PM5G8godwAPMc+VB0mm/KFYSrLIzPBHGiyh507MSwuzIssQPMqWUgcgTy5c6Dn9H44t5by6vhBctapCsaXfZEQLHCskrnLENuZ227QpPmJnryDFZfKPePKzm3i7BIJ7h4FYtPBHHE0kb3Mo8yNpCoAjwWG7J6UGzba7qb2smsSzLBbJE80FkERjLGqkoZ5mG5S5xjbjAI9ooNTRuLtVQW9ze7GS5trmcWsUW1kjtohKJu0JJLSZUbDyAcd+RQV3C11qd7PBeds4dhJMEll7C1ZeycLbwWwy8o3Ou+Zh0Tlu5Ehb6DpWpXeoTy3l+I3s+yhCWahUIlSK5ZcygttOYwcjJ2eyg9KNB5Zxna/py8hsoPOtbSQveTj0N+APJ43HV9uQcdN/sxQen4oFQFAUCJoFigo+NeIPIrcuozK52RL184/vEd4A5+04HfUWXJ1K797o9F6H+Lz9WfNjjM+j92rwTwv5OPKLjz7uXznZuZTdz2A+Pifh0rXDi6vlTzTdKdI9vPY4eGOvCIjv27/t93Wip3IOghcXCRrud1RR1ZmCgfE1iZiObalLXnasbz6FdacS2UquyXMZWMhXYnaoJGR5zYBBweY5cjWsZKTxiVnJoNTjmtbUne3GI7/cskmUqGVgVI3BgQVKkZyD4YrfdVmsxO0xxa8OowPD5Qsi9ltLdoThdo6kk9OhrWLRMb9yS2DJXJ2U18rfbbvc2/yh2rDFtFPcv6kUTZX6xI5fDNRfxFf6YmXUjoPPWf51q0jxmY+H5DZ06W8vFeO8sEht3Urgy7pOfsA/HkQRms1m994vXaEWaun0s1tp8s2vE+G0fnvU/wAnt89vPPpcrZMLM0JPenI7fsYN/abwqPBaa2nHPcvdMYa58VNdjjzuFvX+Rt7l5qnFDxzNbxabezuuPPSNUgOQDynlZVPXHLPMGrTzrzz5QbHVtr35nl08DaOyS+lnZ2OESOO3jQIGY7ejYzknvNB1icN3txob2l5L2l1LExJcjzZM740Zh12kKCeff1oNL5PuLY4beHT79ltbqFViCSkIJFUlEKk8s4UDwPUZzQZuLDquoW8tpbWPk6S/RtcXE8YOzcNxSKPeSGAIznoeVBk1nhPUL21jtbue1EfbwNJHbxSxjyePdujV2diSfMxyXG09aDpIuH4xdSXB2kNBBAsZUbUEMkr5HdzLrgY5bfbyDXs+FoxZS2Mzb45WuSSo2sEnmklAzz85d/X2DlQV2jcBCGOSB72WSCSGS37BY4YY1SUYZsRIN0mP3zz5nxoOgvdDt5rU2ciboSixlckEqoGOa4weQ6UG1Law5WRkTMasisQPMR9u5Qe4HYufqig5Sxh0C1maS3S2E65yLZO1mXI6LFCGZcjuAGaDfGpzMzNa6a5ZwuZrjbao2OQDghpuQJ5GP7KDHNw9c3PK+uyYz1trUNBER6ssuTJJ8CgPhQXtlZxQosUUaxoowqIoVQPYBQZTQRoCgKBYoHQcRrkPlGs2sTc0giM5H8W5v81i+yq1/KzRHhG/58Hf0tuw6Ly5I53t1fh+8rL5RdYktbMvFJskZ0RCAD4seRBHoqa3z2mteEq3QulpqdT1bxvWImZ+nxQPEjw6VFdSndM8ahBjnJK4wvIdfWIHgax2sxii08238BXL0hbBThWJnf0RHP7Q2OFxdSaXHif6d42KyuN2CzMVJB64UgfCs4+tOKOPFHruwpr7R1fIiY4Rw5RG/wAXLvwzv1G3t5pnunVGnuWlJKBPRRETPLzuvvHuqDst8kVmd++XWjpDq6LJlxVjHEzFaxHPfvmZ9Te+U2NFt4bG2iVWlcvsRQoCRIWY4HTu+w1vn2iIpEc/og6DtM576nNaZisbbz42naGtc6sf0db2FuwaeW2VpDnlDD2e9y5HQkeaB7fdnXr/AMuKRzmElNNH8bk1WaPIredvTO+0bfOf+Wnr2pRJpNrYqC8s8cL7VBZlQkSFwB1JIwBnxrW9ojFWnfOyfSYL36Ry6qeFaTaN54Rvy2+8ul0e6vikcNppwtoV2jfcthtoIz9CnnbiB1J6mpq2vtEVrtHp+zlanFputbJnzde078Kx3/8A1PDb6Oxqw4zgzCP6QAr/APz7m9+Cn/TVX/yPY9D1/wDs20/r2j5/d3tWnnkXQHqAcEHn4joaAIoNTU9ItrldlxBHMvcJEVwPaMjkaCjh4Csov2dri255xBdTImT/ALMsU/Cg2F4euV9HVrz3Mto492TBn8aDYXTLsf6wc/Whh/4QKAbTbw/6wI90EX+eaDEdInHOTVbnHgEtEX/6M/jQal5+jk5z6kwx3vftCPiI3QfhQaZ1DhzILXGnuy8w0k0UrA+Id2Jz8aCyi400dRtXUbRQO4TRgfzoNq24q06Q4jv7Vz4LcRk/YGoLSORWGVIYeIII+0UBQI0EaBGgW2glQFByUxCa4hP9baFV9rLIWIHwX8arzwz+uHarHX6Jtt/Tk3n2xt9WK/jF9qghIzDZxln8DNKMAH3DBH1WrFv5mXbur9W+KZ0fR85I8/LO0eqv7/OGvZ8L3ECNPdzLILSCRbZEztULGR2jAgedge338hjFcVqxvaeUcEuXpHFmtGLT16vaWibzPOePL1fni39D4osbaytkknUOIYsxqC75KA42qCR1762plpWkRM90K+q6O1OfVZLUpw61uM8I5+MqzhXVrhp7m4XT55Dcyea52xqsKDagy569c48K0x2t1pt1ea1r9NijFjxTmrEUjjHOetPPl8Fvo9hLNqVzdTIQsSi2hDDkQQGZhnqOZ5+DnwqStZnJNp7uEKeozUxaHHgxzxtPWt8oj88Gw3CdvbW10tpDiSWKVR5xLElG2opY8hkj/Os9lWtZ6kcZRx0llz58U6i3k1tHq5xvPBUWOi6hA8FxFBFI3kcEBWVyrRMijcBgYIzjv8ajrS9Zi0R3RC5l1WkzVviyWmI7S1t4jeJ35L/RotUaTfdyQIgBxFCrEsT0LO/THs61LSMm+9tvY5+ptoop1cEWmfG0x8Ijx9K+JqVz3GcHJ5ReXeodUZhBCfFI8BmX+ElV/Gq+Lyr2v7IdvpGew02LSd8eVb1zyj1xvPwbuqcQ3drM/bWLy2pwUntcyugwMiaD0uRydy5GMcutWHEW+ja3a3adpbTpKo5HaeanwdeqH2EA0GxYX0M6CSGVJEJIDxsGXIOCMjlyNBsUBQczqfHVjFJ2CO9zPz+gtUM8nLAO7b5qdR6TCgjHd6xcehbQWadzXDm4mx49jCVRfvD/AMgm3C8sn7TqV3JnqkTLax+4diA+Pe5oIw/J/pKncbGORj1abdOx97SliaC2stEs4xiK1gQDP6uJFAOefojxoN9I1HRQPcMUGC5vIVC73QB3ES5Iw0hJAQfxZBGPEUGve6JaTDEtrBIPB4kcf4hQUdz8nWlMd6Wohfue3d4GH3ZA+0UFPeaDrFj9LYXz3ka82tL073YdSIp+Rz3AHA9/SguuCeNLbUoyYwY5o+U1u/6yNuh+sue/7QDyoOjNAUBQFAUHIfKHYT4gvbcZktGLFfWjOM8u/GOY8Gaq+es8L15w7fQ2bFvfTZvNyRtv6e788dlvwpeWlwj3NsAGmYNMM+cJAoGGHd09xznvrfFato61e9T1+LUYLxhzf0xtXw237vz0dy7ZQQQQCCMEHmCD3EVKoxMxO8MEOnwoQUjRCFCAqoB2qMBcjuA5AViKxHJvfLkvwtaZ478++ec+ttisozoCgKCE0qopZmCqBksxAAHiSelGa1m07VjeXH3t/NqZNvaFktclZrrGN4HIxwA9c8wW6dfjXtacvCnLvn7Oziw49B/Nz8cn9NPDwm30j8jq7G0jhjSKNQqIAqgdwH8/fU8RERtDk5ctst5ved5njLNiso1JrPCVpct2pRopx6NzbsYZx/eL6Q9jZHsoPHtMbU7fU2k0ntbyJ3IuA1s1tC7Kdp7QsAgk/jUKcgkg5IIe9yyqqlmIUKCzEnkABkknwoPLNPuLviGSRhLJbaVGxQKh2zXhHUM3VU8R0545nJUPRtE0S1s4xFbQJEg7lHMkcssx5ufaSTQWFBTcZaqlrY3M7nASJ8c8Zcjaqg+JYqPjQeZ8AcQXFlpUiTO8l7Jdy29tBIS0hmKxrg5PoKxLsemD7RkLrgnXRYaPamffPcXEswjij8+WeV55D5ue7HnFjyGfEgEOu4d4jFzDLJJEYHt3eKeNmV+zeNQ5AkTzWG1h06HI7qDlLB4ru705IYuziWJ9YmQ5bE1yCIwWPfvklb4cgBQejUCNBGg8l4CsFn13Ur6EAW6F4QwHmvM3Z79p7+aOxP8AGp76D1egKAoCgKBig5TUuDAJTc2M7Wsx67RmJ/rR9B+I9magth49ak7S7GHpaZx9jqq9pT0+dHqn89bJDrWpQcrqwMoH9baMHz/csd2aRfJXzq+77MW0mjzccGbq+i/D/dHBtJxtYZCySPC3qzRSRn7SMfjWe3p38PWinonVbb0rFo/9Zifru204p08/6bb/ABlQfzNbdtj/AFR70U9HauP7VvdJScW6cvW9g+Eit/I07bH+qCOjdXP9q3uli/pbA36mK4n/AN1byYP9twq/HNY7WO7efY3/AOm5a/4lq19do+Ubz8CN9qUvKK0jgHr3EgZsf7qLP4sKdbJPKNvWdjo8fn5Jt6Kxt8bfZBeFRKQ97O90QchCBHAp9kK8j/aJrHZb+fO/y9zaekZxx1dNSKennb/VP02dBGoACqAAOQAGAAO4CpnOmZmd5V2uWlywWS1mCSx5IR+cEwOMpKBzHQYdeanxGVJhh4f4iS5LROjQXMf622kI3p3b1I5SRnudeR9h5UFzQOgqOL7N5rG7hjGXkt5kUeLNGwA+JOKDmPkR1W3m0qGOIgPBuSVB1Vi7NuI8Gzuz7x3Gg76gVBx/Hmkz3k1jbCMm17Yz3T922ABkiYd4dj/h9lBZQ8HWKXj6gsA8pcY3kkgErtLKpOFYgYJAz18TkOL0fgrVYxZOklvE8No1uxlDTPbu8js0sCqdjuylBknHm9+aCw0ngXUIoJrM6inkzpdIMQZmka5D4lnlLZLKWz5uM4oOj4b4bW0knkDA9p2EcYAx2cFvCsaR/b2jcvWoL6gpr3ia0jYoJe1kBAMMCmeUE9NyRglOnVsD20FVcwajfAo4NhbNyYBle9lU925SUtwfYXb6tBfaTpkFrCkFvGI4kGFUfzJPMknmSeZNBtUCoHQKgdACgkKCQoGRnrzoNSbSrZ/Tt4m+tGp/mK1mtZ7k1dTmr5t5j2yzwWcSehGi/VUD+VZiIjk0tkvfzpmfXLNWWgJoIk0ElFA6Cl4k4ciuwjbminiO6C4j5SxN/wASHoyHkR9tBp6JxFIJRZX6iK6wTG65EF2q9XhY9HHVozzHUZHOg6agVByGrfJ5aST+VW7y2dySSZrZgm/Jye0jIKsCeZ5DJ65oM0NprcIwLq0uwO+aJ7aQ+94i6/4PsoNyLUNTH6zT4T7Yrvdn4SRJigmdXux/qyY/Vltv85RQR/TV3/3VcfGW0/ymNBka+vyPMsYwfCW62/aY43oH/wDsmH+iwt/e3AB/9LP4UETpt24G+/dT3+TwxRg+z6USkD3HPtoIScNWzZ7btJ8jBE80kkZ/uSezHwWgsrW1jiUJHGsajoqKFUe5V5CgzGgiaBUBQLFA6BGgYoJCgdBIUDoGKAJoIZzQTAoHQFAUGjrOkQXURimTcuQwIOGRx0eNhzRgejDnQUFnrM1lKtrqDbo3O23vsAJIe6G5xyjm8G9F+7B5UFho3ESz3FzaOhjnt29EnIkhfmkyHAyCMAjuPKgvBQOg09T1W3tl33E8cS+tI6oD7Bk8zQUw4vEv7JZ3Vz4OsfYxH2iW4KBh7V3UEHk1uX0Y7K1H8by3T/YojUfaaDImj6mf1mq4/wBzaRJj70yUEjw7cH0tXvT7ltFH4W9ADh6TP/aV6f7cHP2fqaDE3Dt2B5msXYP8cdpJ9o7EUGCWx1uMfRX1pP7J7Z4j7i8Ln7dtBXXXG15ZnOo6W8cPLNzayeURD+J0wGRR4n7KDrdM1KC5jWaCVZI29F0OQfZ7CO8HmKDaoCgKAoCgKBg0EhQMUEqAJoEOdBMCgVA6AoCgKCj4wtriW3McFvbz7ztkiuGZVaPByFKg+dnGCcY69aDxUjWtMv4b64s5/J4R2HJ1uCLVmJ2PKnNgpbKlwOiig9/vLuOKNpZXCRopZmbkFUDJJz0oPP7TiDUdXLNYnyKxUkG8kUNPNtOG7BG81F5Ebj094IAaGi6jpETb7K1u9UugxRrgxvO+5cjcbqXCIp8UOMMPGg9C0C+upY991aC1cthY+2WYlcDBZlAAPXkM9OtBijv5Xv2hUgQwwK0nLm00znYobu2pExI/2q0FsJFyRkZHMjPMZ6ZHdQa93fRRqzySIiIMuzMAqD+InpQaK6vHPbGazuIX3HZHIxJiMm7aFbHPryx15ig1rTjCwluDbRzhpFJUkK3Zl1UsYxLjYXCgnAPQHwNBp23yg6ZIzCOcuFWRjIscnZZiRpGQSbcM+xGYKOoGaDPwlxL+kI2lFq8cJCmN3eJxKrbsgrGzbGGOatz5ig4Gyt203iNLSzO22vI+2kgH6tDtlJKj93BiyPANjpgUHrtAUCzQOgKAoCgkKB0DBoHQOglQFAqB0BQFAUBQebfLoWa0toC5SK4vIYpWHqEM3PPtAb+wKC645untLKO0soFMlwRZW6cgke6N/PIPLaioT9lBocOaBfwxQWkuoQW6RJ5tvaIO0dVAyXmmJJ5nLFUU5bkRQUOj6pdarIhWW37WyVOxilZuzuLlfo5b8bOckS+cE297cytAW8OqR6dd6nHdvJdTM5aGKNDEpWRbYyKuCxdIosgZGCuCDzyFBp2nGW0u2sJDJPKtvaHshO366dTJNcXciqZ5DzDbQFRSRjBNB193wPFF5QjQzS2/kttuMBHlFzcx3E0jS4yN0gyDg+vgeFBt6Va3lwIMxzrbx3iS5ukiiuWjSCY5kjjxy7XscZAfx6ZoNbQeDL6K3W2Lwxx2q3BtdhZnluZVlRbidsDaoEpwgycnr5oFBcaDwVHbQ2ESt+xl5HwOUs0sLxs5zz6uxHswO6gqIGeyuu1untmnaNo4rTTrdlmuQWVhJMpYnltOCcKu9udBZ8J8OTC4l1K9C+VzgIsStuS1gGMQq37zchuYcs5x1OQ66gKAoCgKAoCgYoDNBKgkDQOgKB5oHQFBFXzyoJUBQFBScY8NQ6javaykqGwyuuNyOpyGAPXwI7wTQcXLYajHCtrqdh+k7eM5jubaTbcrtGFLRsysXwSNyt785yQ39BvNBtdxS3NozKQ5ubaaJyrdVaWZeYPeNxFB0WncRaVtWOC8s9qAKiRzRYVQMAKoPIYH4UG7FqVoBhZ4QMk4EiAZJyTyPeSTn20ELjiGyjGXvLdB/FNGv8zQYU4nsmGY7lJf9zmb7BEGzQH6dDDMdtdSezsGhz8bjsx+NBA3l8+NlpHGD1M8/nr/AHcKurfeCgwvo91L+0X7455S1QWykHuLkvKPerrQbulaPbWwIghVN3NmAy7nxkkOWc+1iTQbjNiglQFAUBQFAUBQFAxQOgdBIGgdAUDzQGaB0CoHQFAUCzQBNBikgRvSRT71B/nQa7aXbn+oiP8Adr/yoJw2UKehEi/VRR/IUGfNAjQRJoFQFAUCoHQFAUBQFAs0DoCgYNAxQOgeaB5oCgdAUBQFAUBQItQFAUCzQKgWaBE0CoCgKAoCgVAZoHQKgVAwKB0BQFAwaB0DzQFA80DzQFAUDoFQRJoACgeaBUBQKgRNAqAoCgKAoCgKAoPCY/lR1ZiFUxEsQABDkkk4AAB5knuoLrV+JeJLaPtp4I0TllgiNs3dN4RyUzkde8gdaCmb5UNWADHsgGztJhwGwcHac88Hly7xQQPyq6p60P3X50GRvlO1cNsPZh87dhgIbdnG3bnOc8seNBFvlS1UEgmIEHBBhwQQcYIzyOe6gj86uqetD91+dAfOpqnrQ/dfnQP51dU9aH7r86BfOpqnrQ/dfnQP51tV9aH7r86B/OtqvrQ/dfnQHzrar60P3X50B862q+tD91+dAfOtqvrQ/dfnQHzrar60P3X50CPyq6p60P3X50APlV1T1ofuvzoH862q+tD91+dAfOtqvrQ/dfnQL51tV9aH7r86A+dXVPWh+6/OgPnV1T1ofuvzoF86mqetD91+dAH5VNU9aH7r86A+dTVPWh+6/OgPnU1T1ofuvzoD51NU9aH7r86A+dTVPWh+6/OgPnU1T1ofuvzoD51NU9aH7r86A+dTVPWh+6/Og57hXUktryC4dSyRvlgOZwQVyB3kZyPaKDotQubaK1jC3FtLIUlWVojK01wHhZFExZAV5lSdxBAGBmgzQcY2D7Y5bFRGp81jHA2xTI0pjCLGAFBKqCBlhndnPINKLiqxBBOnK2Bggpbjc2wgznEXKRicFQdg6gZoN0cY2J2yNaefG4CR9nDjsg88gAcpmMAvF6JDZQYOM0Gva8Z2ySCbyFe13l2YLCMkuAGHmEqyxr+6QGaRy2aDQ1LiC0kgliSyVHcJsk2wqY9rAlVKIDtwDzOWJc5OAKDmaAoCgKAoCgKAoCgKAoCgKAoM1rPsJOxXyMYcZHUHOPHl+JoNx9Tj5YtYQQQc4JHLHLB9xznPXuoFFqaqSfJ4jkg4ZQQMDGB7+p9uaAk1UH/R4RyI5IB1AAPvGPx55oNW7ud5zsVOWMINo65zjx5/hQbMWpqAB5LAcAAkq2WwCNxO7qcg/AUGT9LJgDySDlgZ2nmAMc+fPu5/586CTayhGPI7fr6p6A8gOef591Bjk1RCc+Swjr0UgHJU9M45Y5e8/ENS9uBI24RpHyxtjBC9Sc8yefPHwFBgoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKD/2Q=="
                  title="MIME"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    MIME(MUltipurpose Internet Mail Extensions)
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Son una serie de convenciones o especificaciones dirigidas
                    al intercambio a través de Internet de todo tipo de
                    archivos.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://i.blogs.es/3951b6/error404/450_1000.jpg"
                  title="Métodos y códigos de errores HTTP"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Métodos y códigos de errores HTTP
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Métodos de petición es para indicar la acción que se desea
                    realizar para un recurso determinado. Los codigos de error
                    indican si se ha completado satisfactoriamente una solicitud
                    HTTP específica.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>

          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/1200px-HTML5_logo_and_wordmark.svg.png"
                  title="HTML5"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    HTML5
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    HTML significan Hyper Text Markup Language el cual es usado
                    para estructurar y presentar el contenido para la web.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>

          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://miro.medium.com/max/800/0*aH8YUI7nqAZ6b-V_.png"
                  title="JS"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    JS
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    JavaScript es un lenguaje de programación interpretado, el
                    cual es orientado a objetos y este se usa principalmente en
                    las paginas web con el fin de agregarle dinanismo.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1200px-PHP-logo.svg.png"
                  title="php"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    php
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Hypertext Preprocessor es un lenguaje de código abierto para
                    el desarrollo web y puede ser utilizado junto con HTML.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKYAAAEwCAMAAAAKHvqvAAAAz1BMVEX////lbx9TgqHnbwBOf59He5xCeJrkZgBKfZ3jYgDnbQBPgJ9GepzjYQDmaQDlYADlbBX4+vvh6O3zxbHupoPK1t/lbRj1zr7X4Ofz9vimu8vkaAVtk619nrXp7vJZhqS/ztmKp7yYscOzxdJzl7Cqvs377ufqj2D54NLrl2r9+PXxu6Hwr4jtm2j42smOqr776+PnfTzsnXTwsZLpfS3rjE3odx3pgTYwb5Tog0jpilXslV70wqXuoXLxsIrriUfwtZjpfi7kVQDmeDLto3zcmhm5AAASKElEQVR4nO1dCXeiyBaOCgW44JK4gyhqTMhETdKdvbvtl/z/3/SqWC8Um4IWfY7fnJnplu3jVt21LnBxccY/iGvWBNLhbsuaQSrcVFkzSIfmjjWDVHhuGqwppMGH8B9rCmnwLQusKaTBkyxcsuaQAp9y9YM1hxT4FKr/wqh/yhXphjWJZOzkyr8wOX/LFfk3axLJuK9W5F+sSSTjBdP8B1RdqFSqP1mTSMRWqvwL0vwk0rxnzSIRP6tYmoUPPgw85hXhkzWNJJAxrwhr1jSS8BePeaXwgfFNA7Os/mBNIwlEgYrvK01hFj9A+kGEWa2wppEAU80LP+bXxGZiPS94feaFDHnhPeV/giXMYheR+k2TZcHTyq1pi4o+M42qOTEr8jdrJnEwni2W1WKXDW2WlWahYyOHpVxkY+SMeLHd5HXFZllpFjjm2MoOS6nAucW64bAs8sS8a9okK9UX1lyi8UtyWVYKmwAZL4LLUk5wkmtm6nVTrbospdi4qP/xwozlZ9Nj2Yhh0f9oCv3T0QrgV6OSguX6l9BosqvVGH+FZJbXTxVJlv6wi5PXrk3HLIVwluuPplyVZYZ1+F0TsJRDpfX5LMmVavOeoZn6cK0lsZchlsjYyQIpIFYYxnXXz7LHUn6hxWX8FsicqDZZrhSsBW/AK1JIgvZkkqzIVZYh8h2cls0navunbJLEs5IBORe/4LSUKKt988OSdZWhQcf441lLrCBB5THubVELITP2dDB+AOURqMWfvizbc4HpEsF1BbBsUAm5I8oq29L7FnieSjOYUGyde5CfmdY9tgJU8aCGXDoGgJ4LJ8U19OJU1eC3k2tIbBMiowpZBmONe8dMSYzX157hvAzK8sMxUxLjevZPoOPUvPRYMpblf8CqS7vAxm9no8A4UV83PZZU0eDT2Vj9w4ScB6jkwYW+rctSZpyo/4LOJ2i7Xd1qMC5tbsGQUyvlv52Jybxx4j5myA33FlivsxhAmJRdd+cDc2F+goQ86K+9W2AbBmN8eGNOrUDfucolsa7HVbxKEdV18NfZxr7q7pWK5LvAJm/MmZv2a49mI6jMfa+8ybrdzKNJD+zOnZrMu1DAwFI2B7gn5n267sDKVO3gw6PJvA3lxdFmuqXsp2ermLfFfTsiozuGgTSZL/266kzThKET87ZnJx2jB/0J0GRu4J0Iie4v6oPcoyLsGHADuLEtJx0EXYPgib1bt3U9ZFhhWszcE63t2Uk5S9KKD8XJWItsccqUDm19o16V2Db33NidUPRS9EvVx5PxWrVtIOl8py9BmhWBcXHGKnSFPGvhUyLm3R523aNB2ZyAOFkbeauKFNJa9scvTtYNHy/m9KSKHgETz3zYDWtVik7Hd4KPJmttt6xSSBOc3yhV4hoVTgGzQhiiI4bg48n80StTjULC9LXfF7EOkC/uhXAd2fmsEvt+2D9Y3avP9O/3MARhnr1ZZimsL/cvmJ5FeAyHOMcQbQe1kULQNJ7lUCUBkScdljIA4RlWc3WtUkEeVCYtug06Tv8uTN3YBuZZpdOzdXQ6zwjGD1mm6pmGrURVmQWlcLzIdB+xXVUu1MOLfwRq2H9YLTOsix9+fDSDmmKlG0XrKb//X4CQqULFe6fHt394yZPUFZl1CT4Ev32Wx3ySOk0Fvj2YjvSNPpoOjsQrCJ/vJg/5PycWuyZLBSFeFDlR5BFSp3nQGA6689FG1bvt5H1vGhX5b9JObfX1lVMUjUdimYCri/MsBNuD+UbhET7pJg3HC9IMktyANFT07tD802DE8SbRMq91DqU4IgPD80iZT9IetG6m6Isbwr/Mec6SKJruz3E4X+Cpw9V4tJgOk3d3YMj7rwC3F7ZA0Z4DP5wriNwjh7T5HhwxXg5qhJw5PLt7HNRVTY7lGlL3NRX3B/pxlbN5phVKe1lG1jG8svec7h+cSto0OTXV3kMd8bXDJkpKtMPtxdQZ9hS62t7YZgwjmx0LxaS7VGdR06hsSUfUE08zqLsky+V6jh5sgr2iqiH0uoieRiPr2rVy4slQzWOJ98/Oc9jpznVVq2PbK3IcUuLOOHBGPUmJpqgMUUPacpDO8QTJEdc6W5R5ZPKzR5OLNzZD++p8knTaGl/2E8VBgaaO5t3BZBjNtz2cdDCx6Xypb1RFE01yRHreiTge8Ulzrm3TrCebTh3xXDlAlROJtyTBTFlTlMVCtbBY4MBBq1nbTGIk2uG4GnV4HZVn82TTtgdNy/uIQareNbka/tcE+X+tFrGjc38IldVlN53Fntg0UTpjPZzqZiDIRZFNgCl+LF9tNpp29pjaXUeF9jim0x3NFHuSifFSs6RMglu+TkZfUfXldLBfIECg2wZJ2/vI9oSorK4ulHIdOeBtuD8gTlPUmT6aTwed/dm5sG9ZHB1+CpPzcDghKu2i05lMhjEWYD90namZOrBlAsUOjdOFHqzwjwjT1tJEJ8AWs4PV3IcjD4UTTaSO3cMwHCl56XM4Bvs5oPBzqKiWH6MwdByWh0eOkwXi+H3SvQMuUTfVh8vAckSyt3287P4YWMG4WD58/qt81imTCNtgogxmXa9bnkE7UAHbnam+qHNxYhoha8Cnh13BhJN0cEjfU6BDkqOVzXgQJyuRc7u9IIKoITWLIWp7uZGIAzQcn3WTQiCSo+EckjcLl274h2bhs3tQwzO/xmsZc0MV5MNlElHW7bBto4+W8+m0a2GK06DRZrbQOARzNHgsH5b061h5avXyNBtJksOJwQuCIBiCpEGxwXO9PgpM0QFOEDmkZSZJMEMHphr++8IJqu5PidozVBPRIi+L3FGRmJBixFPEiaW2oQqkcxz9l4PyzYTJUqPT4lQyxJmluBiFJJbd8qu4yX/1YjJXORSmGjEESR2iGyqujsbrR1thmXTNYlVUplnDibtoZb7lxWbZjUl905fxDyc7mC51bHqwIYXgsZVazPQlKeMcncOeMFPNPJPLM844gz06ufq246A91zLl0KfBEnufjAW942NClg4KXiojRXEzPuJn+406TtdO1cJhYuGseiJlmapY3ekuZyRKEZF6Qq3zEjczQDNr693OhPLkZglct/PJmn1nm5MpXrkWCNns3A3xOCyyF4kUTUT2qpU/wBPR5kQSDawRBoJMe5EoOmLm9u8dOAx6JM90IJ0Yp+A5z5Rfiqi8PM0MHaqHEuV4/gjJWSQmG7e5Ij1FER0jgYxHe0r6ktJSJbZLWR6z4mhyCs1v2l1dQ3U+PhHmyNK0qC6PKkZSkdTQK0Z5EyqL4QC7GN5e2HdTYS//5ZXZMjw/zwt2RxzOtPGVOsOIThwTOBEmHYiKphHpcWUz/x3NEwuLEUh/VHfGIywfHqlTBuHvLNUs7ug8KROJaDFlk3QPXpNrc13FbJISkc4sjRiipORgqln1NqQzrF5M+fo0bntHs3ukxFPbYYh2rRa7qqM7TjCxSemY6GCfOovZrrj9UehknCgMN6hWX8TsoHgrAOLmZLT86KrYDqK4tHUEe81E7fS2aDKd8XWOQ4tYC6P5fHONR7OULeM5oD2YzziEwwNsqxPUQgnGkNgHKaODmgz3IYgTT1VDpj/heJG21UHWA0SHOiS80Y4TPJjNnAsv8eTqePjo3VTKMg200FDX7BLkNVU/OJTw0B52SJQyU+xmzpo7btwmzD+2lbBu2O4issHQacnjNLP4P+0OOnFdnPZVSDPnoNvFzPSZqpTdtJgD5+VRXY3olJzyEcvFONTF4RuOdSOTnZoVStZ5ZK+cljXYyGl2cpKIrman7wjvajdzUneOb1yLbuYcKLwYV6qadOcbRbRmTYo0wmzidBo5rZbOhMPsFa3ZPKa6gyMgjkvRiER0cGPOcf7w9k3qhsypYzZzxl17Mqph1ef36SSyakFKGaXt3wyCrGRb3ZzaYjNKbuYcTq1QEsU59pjDTS3VZwuTsj3pCHECZ8w582/mBmv61sukLyB1N+fEfkqlzIt5LFsTFR6QToT5cqTr+gZjhv/ZbPBfRlazwoAYg73O2dXL9vKyGOvX2WEyxdm0bUixBRgVb0lxiN2l6NYmsFtXpqwp+dHuTEcLbN5dl4IDHm15sGceHtJrHYe26dBN0+yaDfKQwiLb6np7rr2azzlk8+pWr7fZm16HBSjT2i9yKeAM5yp6fcUWRsQGZpTSqV9YTt2yY5rjXuHjGVYBX4+39ntiYj5S4FhMp83dqrkTY2RjZtbfFbNfyt476BVq1tOp6HglJrPhTas7Pok48Bow7d5DGkFn5TRWEeragQ8X7A37mQLik3jngYI6T6PuPGAgmm1qSzxb6NWZE8F6SIhElgRzE+Yfu13LGRXPWp9xxhlnnHHGGWec8a/BMFh/xCEJN3e3JUGSJGH8dbtL8Ropw8j7nm6aX++3V7+ffl89rr5C32R1/dSSxq1eyUSvNZaEx8BuW3KO76unp6vH29VDaSyQW5IEufT2uMvphYLXUq/Vao3HY/zfXsj3XY3vxrgUQEt68xE1Gt45nPtxbkqQbvNgakjgtPRLp9eyS7IHCPT8H5GD56CAbyqHF3MKkGZwQj017C14qL8eSg3BId0rwd188iYibfmJNrJ/6EWOoXlli2ncurOmw82VYDEY+z5EUoIj/YixKkk+6s3MLxaEZ5P8m+5slsKj95uxMsXvf4/xg0ez92D9tL1qQO6Zv1b/Bc7mfzXl1h5x4cr385PkkbHxTtPEh/u0SchooMAlSmPfFltGrffAEY/j0njn++XWm4u9N/fXa6hZrdtsNFdwusMNffsqtJXqBX97DKV5cQnVk37d9V6AkvANpT0b4HXdG2j5f/gOp3kBJRAYgH0RIYmLG0fLr+hjmoF37l6Nw8/xBNQ95Hb3AbiEbxY6l0gjBUCnB89x4zPKmWiCS7RW4Pc3W7VaIdIMYhdB02hAmpkmZxRNx+ynGay7iBHx+Y6ID2+nBJAENBqeIFJI4c6j47tVn1HO9i7rCJpbKezXCHwKETTfcqMJBww4RY9mSQh+SIzCZSqamV4SDQcsnGZJor9InZKmb9AzuUt4CWAOfZ5OWEUfHzyHb4pAg9SKOnpvmtD2wCvgGDI2YuxH0IQxdyvhVg+k+QDjGzxkbzEaEEVzDe6V+lRePjSvAilQr7GK1IEomsDXZw09omjeUAlOq7GKkCikCUNoOObBcDAnmj4tdXaQ3kKj8Aiaj1CYGd8SHUlzDR2yO/TSe8jQh9Ps56dAMTRJlB6CVoO2oqE0++A2e62sRZBomhdf/jzWgfAQ1IYQmsYtkGVYnSI/mkaLnp7mfsFgJ0DT2F7eSuAWW3L2jynE0LwwSuHyDCpE3+cLxpIggOPwdM6h7BVHEyd0PgIe/NFdP2IvU/KlXL5tG0/z4lIKFai/OBNBszcWGu85fYA3gSbWhUYYUV8mB2n2BBuS/HbVz63KmUQTx3SrMKLQ+QGavfd1n2C9zbcOm0wTE32kiUJxRnihU9PE4SdNFHwXIyr0ODlNTHTVCIR2njEsEE0cM335vCd4hfqJaYZ858iHRxjcgXLIiWkmTv87EE6MvRAkMhfKDxGZZdTenjyBqkdmljnSDC8nRMErh0ZIsxg0vfwdzM1LaN6PQ3MXUeqKQpimg4lzLJpRFbkouNVlYDfvIsqwR6Lpk8T2KzS4uXJogmWP3fFpXkWUUC/6zdCp6qSLcCLDavFD2EHZAcv7Ppq7cehajlMNkUAq/Pv4NG+jaK5a45BKnFOe7X2BH+GtftHH5IFVFM1WaKHY0RYJTtzHqLWl/AAX8iBNUjmkRWMIYdoGRqR0pO+egcqb79qmyW49BKbnm0WoN/b9/h657pkbwHD57KY1kK0xHNzrB1tsgfIALDI2jkMTrIn4Qg/nysKXvZZurB/twLgXrPaXcqtdRwIkhVCzPd/dG0tS6eFr3HBqBMJXULNguHycr3/DGiT8OOFlswThNXq0GvRns339Ikf5qiVcCvCVhi57Ap329saNW1paVueMBWpC5ANYFA7M/v5KENwGJKtZ52sXNvO2vfeVg/fSUT5lCqr4IXXn9d3tQ8tsfhp/rZ4O/+xRZoCINvK7mQXojgPBTbYF7+Pi3cttUiycMwMYc+YjGw0vi8mhQexo2LrCFAo85Ddj13Mfqd6XA4xvp9TSa+5Yk4nEzu0fFUoF+54rhG3Ye0LGJeQjg8RGLaHxVpRvNkfhrSmv7ph/6D4R2yJ87/yMM84444yD8H82VJLFDNia7QAAAABJRU5ErkJggg=="
                  title="jsp"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    jsp
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    JavaServer Pages enfocado para crear páginas web dinámicas
                    basadas en HTML y XML. Es similar a php pero es basado en
                    java
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
          <div>
            <br></br>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://www.ooisolutions.com/images/AJAX/Ajax%20develoment%20side%20image.png"
                  title="Ajax"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Ajax
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Asynchronous Javascript and XML. Se define como una técnica
                    para el desarrollo de páginas web que implementan
                    aplicaciones interactivas. Permite programas escritos en
                    Javascript, que un servidor y un navegador intercambien
                    información.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}
