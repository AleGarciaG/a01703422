import React, {useState} from 'react'
import {TextField, Button} from '@material-ui/core';
export default function TextoDinamico(){
    const [texto, setTexto] = useState("")

    function handleChange (evento){
        console.log(evento.target.value)
        setTexto(evento.target.value)
    }

    function mostrarTexo(){
        console.log('Estamos en mostrar texto')
        return <h2>Mostrando texto</h2>
    
    }

   

    return(
        <div>
            <TextField
                id="standard-multiline-flexible"
                label="Introduce un texto"
                multiline
                rowsMax="4"
                
                onChange={handleChange}
            />
            <Button variant="outlined" color="primary" onClick={mostrarTexo}>
                Da clic
            </Button>
           {
                MostrarTexo2()
           }
        </div>
    )

}
function MostrarTexo2(){
    
       return <h2>Mostrando texto</h2>
    
}