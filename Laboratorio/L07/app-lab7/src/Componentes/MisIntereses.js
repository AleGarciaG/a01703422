import React from "react";
import { Container } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

export default function Home() {
  const useStyles = makeStyles({
    root: {
      maxWidth: "100%"
    },
    media: {
      height: 480
    }
  });
  const classes = useStyles();
  return (
    <Container>
      <Grid>
        <Grid>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            textAlign="center"
          >
            <Box textAlign="center" m={1}>
              Mis intereses
            </Box>
          </Typography>
        </Grid>
        <Grid container spacing={3}>
          <div>
            <br></br>

            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://lh3.googleusercontent.com/proxy/QlxRw3aR1KNgJVPBxuavMsn-ghNfepUfy2EpjoPj7Ye5cNfddMazSuzbKateN94Zh3P4iTDwNUJ77VhVfcSz2w9NNQWuavlIQc-qSHUe6-dWa9jJ68F7l0xxH5UvTRplF2uRtknyEck7"
                  title="URL"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    URL(Uniform Resource Locator)
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Comencé mi carrera como Mecatrónico, sin embargo, debido a
                    mi participación en el Grupo de Robótica <b>Eagle X</b>{" "}
                    desarrollé un interés en el ámbito de programación, lo que
                    me llevó a cambiar mi carrera a{" "}
                    <b>Ingeniería en Sistemas Digitales y Robótica</b>
                    Como miembro de Eagle X he participado en dos ocasiones en
                    el <b>University Rover Challenge</b>, una competencia
                    internacional, la cual busca que los alumnos desarrollen
                    habilidades tanto profesionales como personales, lo que me
                    ha ayudado en mi carrera, fortaleciendo mis capacidades en
                    cuanto a procesos de autoaprendizaje y trabajo en equipos
                    multidisciplinarios De manera personal, también he
                    encontrado gusto en el desarrollo, ya que me gustaría poder
                    realizar algunos proyectos en los que pueda aplicar estos
                    conocimientos tanto de manera web como en aplicaciones.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
            <br></br>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}
