import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },

  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
}));

export default function SimpleExpansionPanel() {
  const classes = useStyles();

  return (
    <Grid>
      <Grid>
        <Typography gutterBottom variant="h5" component="h2" textAlign="center">
          <Box textAlign="center" m={1}>
            Preguntas
          </Box>
        </Typography>
      </Grid>
      <Grid container spacing={3}>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta1"
            >
              <Typography className={classes.heading}>
                {" "}
                ¿Cuál es la diferencia entre Internet y la World Wide Web?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                El Internet por su parte, es una red global de comunicaciones
                interconectadas definidos por los estándares TCP/IP., mientras
                que la World Wide Web (www), es una recopilación de información
                determinada como recursos a la que se accede a través de
                Internet
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>

        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta2"
            >
              <Typography className={classes.heading}>
                ¿Cuál es el propósito de los métodos HTTP: GET, HEAD, POST, PUT,
                PATCH, DELETE?{" "}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  HTTP define un conjunto de métodos de petición para indicar la
                  acción que se desea realizar para un recurso determinado
                </Box>
                <Box textAlign="justify" m={1}>
                  GET: Solicita una representación de un recurso específico,
                  sólo deben recuperar datos. Los datos producidos se devolverán
                  como la entidad en la respuesta y no el texto fuente del
                  proceso, a menos que ese texto sea el resultado del proceso.
                </Box>
                <Box textAlign="justify" m={1}>
                  HEAD: Solicita una representación de un recurso específico sin
                  el cuerpo de respuesta. Este método puede usarse para obtener
                  metainformación sobre la entidad implicada y se usa a menudo
                  para probar enlaces de hipertexto para verificar su validez,
                  accesibilidad y modificaciones recientes.
                </Box>
                <Box textAlign="justify" m={1}>
                  POST: Se utiliza para enviar una entidad a un recurso en
                  específico, causando a menudo un cambio en el estado o efectos
                  secundarios en el servidor. Así mismo cumple con otras
                  funciones tales como extender una base de datos a través de
                  una operación de agregar; proporcionar un bloque de datos como
                  el resultado de enviar un formulario a un proceso de manejo de
                  datos; y publicar un mensaje en un tablón de anuncios, grupo
                  de noticias, lista de correo, o grupo similar de artículos;
                </Box>
                <Box textAlign="justify" m={1}>
                  PUT: Reemplaza todas las representaciones actuales del recurso
                  de destino con la carga útil de la petición. Si el URI de
                  solicitud se refiere a un recurso ya existente, la entidad
                  adjunta debe considerarse como una versión modificada de la
                  que reside en el servidor de origen.
                </Box>
                <Box textAlign="justify" m={1}>
                  PATCH: Es utilizado para aplicar modificaciones parciales a un
                  recurso.
                </Box>
                <Box textAlign="justify" m={1}>
                  DELETE: Borra un recurso en específico. Este método PUEDE ser
                  anulado por intervención humana (u otro medio) en el servidor
                  de origen.
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta3"
            >
              <Typography className={classes.heading}>
                {" "}
                ¿Qué método HTTP se debe utilizar al enviar un formulario HTML,
                por ejemplo cuando ingresas tu usuario y contraseña en algún
                sitio? ¿Por qué?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  MÉTODO: GET o POST{" "}
                </Box>
                <Box textAlign="justify" m={1}>
                  Debido a que este método es utilizado por el navegador para
                  pedir al servidor que se envíe de vuelta un recurso dado; este
                  método es usado en formularios, sin embargo para la solicitud
                  de contraseñas y datos sensibles no es recomendado su uso, ya
                  que el texto colocado será visible en la barra de navegación
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta4"
            >
              <Typography className={classes.heading}>
                ¿Qué método HTTP se utiliza cuando a través de un navegador web
                se accede a una página a través de un URL?{" "}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  MÉTODO: GET{" "}
                </Box>
                <Box textAlign="justify" m={1}>
                  Debido a que el ingresar un dato de dirección en la barra de
                  navegación, se genera una petición de recursos, por lo que el
                  método GET es el que devuelve la respuesta a la solicitud.
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta5"
            >
              <Typography className={classes.heading}>
                Un servidor web devuelve una respuesta HTTP con código 200. ¿Qué
                significa esto? ¿Ocurrió algún error?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  CÓDIGO 200: OK{" "}
                </Box>
                <Box textAlign="justify" m={1}>
                  La solicitud ha tenido éxito. Dependiendo del método HTTP
                  usado, este éxito puede definirse como:
                </Box>
                <Box textAlign="justify" m={1}>
                  GET: El recurso se ha obtenido y se transmite en el cuerpo del
                  mensaje.
                </Box>
                <Box textAlign="justify" m={1}>
                  HEAD: Los encabezados de entidad están en el cuerpo del
                  mensaje.
                </Box>
                <Box textAlign="justify" m={1}>
                  PUT o POST: El recurso que describe el resultado de la acción
                  se transmite en el cuerpo del mensaje.
                </Box>
                <Box textAlign="justify" m={1}>
                  Este código está catalogado dentro de los denominados:
                  Respuestas Satisfactorias
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>

        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta6"
            >
              <Typography className={classes.heading}>
                ¿Es responsabilidad del desarrollador corregir un sitio web si
                un usuario reporta que intentó acceder al sitio y se encontró
                con un error 404? ¿Por qué?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  CÓDIGO 404: NOT FOUND
                </Box>
                <Box textAlign="justify" m={1}>
                  Este error se da cuando el servidor no puede encontrar el
                  contenido solicitado. Si bien puede darse el caso que el link
                  dentro del archivo HTML se encuentre dañado, puede darse
                  también el caso de un error del usuario que no colocó los
                  datos correctos en su búsqueda o que en caso de ser un vínculo
                  o un recurso a una página externa, esta haya cambiado y por lo
                  tanto ocasione este error.
                </Box>
                <Box textAlign="justify" m={1}>
                  Este código está catalogado dentro de los denominados: Errores
                  del cliente
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta7"
            >
              <Typography className={classes.heading}>
                ¿Es responsabilidad del desarrollador corregir un sitio web si
                un usuario reporta que intentó acceder al sitio y se encontró
                con un error 500? ¿Por qué?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  CÓDIGO 500: INTERNAL SERVER ERROR
                </Box>
                <Box textAlign="justify" m={1}>
                  Este error se define como que el servidor ha encontrado una
                  situación que no sabe cómo manejarla. Algunas de las razones
                  por las que se da son: problemas de autentificación, errores
                  de código o saturación del servidor. Por lo tanto este error
                  si es completa responsabilidad del desarrollador
                </Box>
                <Box textAlign="justify" m={1}>
                  Este código está catalogado dentro de los denominados: Errores
                  del Servidor
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta7"
            >
              <Typography className={classes.heading}>
                {" "}
                ¿Qué significa que un atributo HTML5 esté depreciado o
                desaprobado (deprecated)? Menciona algunos elementos de HTML 4
                que en HTML5 estén desaprobados.
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  Un elemento o atributo en desuso es uno que ha quedado
                  desactualizado por construcciones más nuevas. Estos elementos
                  son definidos en manuales de referencia, pero están marcados
                  como obsoletos.
                </Box>
                <Box textAlign="justify" m={1}>
                  ELEMENTOS RETIRADOS DE HTML POR SU MEJOR FUNCIONAMIENTO EN
                  CSS:
                </Box>
                <Box textAlign="justify" m={1}>
                  basefont,big,center,font,strike,tt
                </Box>
                <Box textAlign="justify" m={1}>
                  ELEMENTOS RETIRADOS QUE DAÑAN LA USABILIDAD
                </Box>
                <Box textAlign="justify" m={1}>
                  frame,frameset,noframes
                </Box>
                <Box textAlign="justify" m={1}>
                  ELEMENTOS REEMPLAZADOS
                </Box>
                <Box textAlign="justify" m={1}>
                  rev & charset ---> link & a. shape and coords ---> a. longdesc
                  ---> iframe. target ---> link. nohref ---> area.
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta8"
            >
              <Typography className={classes.heading}>
                ¿Cuáles son las diferencias principales entre HTML 4 y HTML5?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  La creación de atributos como header, section,
                  article>,favorece a que las páginas sean más semánticas y
                  mucho más fáciles de leer. HTML5 permite incluir elementos de
                  SVG y MathML; separa el desarrollo de estilo de de desarrollo,
                  promoviendo el uso del CSS
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta9"
            >
              <Typography className={classes.heading}>
                ¿Qué componentes de estructura y estilo tiene una tabla?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  Una tabla básica puede ser declarada usando tres elementos,
                  table (el contenedor principal), tr (representando a las filas
                  contenedoras de las celdas) y td (representando a las celdas).
                </Box>
                <Box textAlign="justify" m={1}>
                  Los títulos de las tablas son insertados mediante el elemento
                  caption, justo después de la etiqueta de apertura de la misma
                </Box>
                <Box textAlign="justify" m={1}>
                  Los cuerpos de las tablas tienen dos particularidades:
                  primero, que puede haber más de un cuerpo en una misma tabla;
                  y segundo, que el elemento tbody puede ser omitido cuando la
                  tabla tiene solamente un cuerpo.
                </Box>
                <Box textAlign="justify" m={1}>
                  En términos de estilos, estos pueden darse desde un archivo
                  CSS
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta10"
            >
              <Typography className={classes.heading}>
                {" "}
                ¿Cuáles son los principales controles de una forma HTML5?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  form: Representa un formulario, consistiendo de controles que
                  puede ser enviado a un servidor para procesamiento
                </Box>
                <Box textAlign="justify" m={1}>
                  label: Representa el título de un control de formulario.
                </Box>
                <Box textAlign="justify" m={1}>
                  input: Representa un campo de datos escrito que permite al
                  usuario editar los datos
                </Box>
                <Box textAlign="justify" m={1}>
                  button: Representa un botón
                </Box>
                <Box textAlign="justify" m={1}>
                  select: Representa un control que permite la selección entre
                  un conjunto de opciones
                </Box>
                <Box textAlign="justify" m={1}>
                  textarea: Representa un control de edición de texto multilínea
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta11"
            >
              <Typography className={classes.heading}>
                {" "}
                ¿Qué tanto soporte HTML5 tiene el navegador que utilizas? Puedes
                utilizar la siguiente página para descubrirlo:
                http://html5test.com/ (Al responder la pregunta recuerda poner
                el navegador que utilizas)
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  476/555 puntos
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        <div className={classes.root}>
          <br></br>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="pregunta12"
            >
              <Typography className={classes.heading}>
                ¿Cuál es el ciclo de vida de los sistemas de información? ¿Cuál
                es el ciclo de desarrollo de sistemas de información?
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <Box textAlign="justify" m={1}>
                  Ambos ciclos se dan de manera paralela ya que son
                  codependientes. Este modelos, conocido también como «modelo en
                  cascada», se basa en intentar hacer las cosas bien desde el
                  principio, de una vez y para siempre. Sin embargo, este
                  proceso raramente se lleva de manera lineal, ya que en varias
                  ocasiones este proceso requiere que se regrese a pasos
                  anteriores
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de planificación. En esta fase se prepara el diseño y
                  posterior implementación del sistema. Es necesario definir el
                  alcance del proyecto, justificarlo y escoger una metodología
                  para su desarrollo.
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de análisis. Se decide por una metodología de desarrollo
                  determinada
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de diseño. En este estadio el equipo de proyecto tendrá
                  que determinar cómo el nuevo sistema de información cumplirá
                  con los requisitos aplicables.
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de desarrollo. El desarrollo software marca un antes y un
                  después en la vida del sistema y significa, además, el inicio
                  de la producción.
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de integración y periodo de pruebas. El objetivo de esta
                  etapa es corroborar que el diseño propuesto cumple con los
                  requisitos de negocio establecidos
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de implementación. Someterlas a pruebas, crear la
                  documentación pertinente y capacitar a los usuarios.
                </Box>
                <Box textAlign="justify" m={1}>
                  Fase de mantenimiento. Introducir los ajustes necesarios para
                  mejorar el rendimiento y corregir los problemas que puedan
                  surgir
                </Box>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </Grid>
    </Grid>
  );
}
