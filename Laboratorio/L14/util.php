<?php
function connectDb($servername = "localhost",$username = "root",$password = "",$dbname = "dbname"){
    $con = mysqli_connect($servername, $username, $password, $dbname);

    //Check connection
    if(!$con)
        die("Connection failed: " . mysqli_connect_error());
    
    return $con;
}
function closeDb($mysql){
    mysqli_close($mysql);
}
function getFruits(){
    $conn = connectDb();
    $sql = "SELECT  name, units, quantity, price, country FROM Fruit";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);
    return $result;
}
function getFruitsByCountry($fruit_country){
    $conn = connectDb();
    $sql = "SELECT  name, units, quantity, price, country FROM Fruit WHERE country like '%".$fruit_country."%'";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);
    return $result;
}
function getFruitsByLowUnits($fruit_units){
    $conn = connectDb();
    $sql = "SELECT  name, units, quantity, price, country FROM Fruit WHERE units <= '".$fruit_units."'";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);
    return $result;
}
?>