-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2020 a las 02:39:45
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Base de datos: `dbname`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fruit`
--
DROP TABLE IF EXISTS fruit;

CREATE TABLE `fruit` (
  `name` varchar(40),
  `units` int(11) ,
  `quantity` int(11) ,
  `price` int(11) ,
  `country` varchar(40) 


) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fruit`
--

INSERT INTO `fruit` (`name`, `units`, `quantity`,`price` ,`country`) VALUES
('pera', 201, 1,2000 ,'Japon'),
('manzana', 101, 1,50 ,'China'),
('manzana', 10, 1,100 ,'Mexico');

