<?php 
  require_once("util.php");  

  include("_header.html");  
  include("_navBar.html");

  viewQuery(getFruits());

  include("_footer.html"); 
  include("_endPage.html"); 


  function viewQuery($result){
    if(mysqli_num_rows($result) > 0){
         print" 
         <div class='container pt-3'>
            <table class='table'>
                <thead class='thead-dark'>
                    <tr> 
                        <th> Nombre </th> 
                        <th> Unidades </th> 
                        <th> Cantidad </th> 
                        <th> Precio </th> 
                        <th> País </th> 
                    </tr>
                </thead>
          ";
        while($row = mysqli_fetch_assoc($result)){
            $name=$row['name'];
            $units=$row['units'];
            $quantity=$row['quantity'];
            $price=$row['price'];
            $country=$row['country'];
            print "
            <tr>
                <td>$name</td>
                <td>$units</td>
                <td>$quantity</td>
                <td>$price</td>
                <td>$country</td>
             </tr>";
        }
        print "
            </table>
        </div>";
    }
    }
?> 