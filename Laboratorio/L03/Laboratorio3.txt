Es opcional
Laboratorio 3: CSS
 Descripción
En esta actividad se hará un breve repaso sobre CSS.

 Modalidad
Individual.
 Objetivos de aprendizaje
Introducir las hojas de estilo en cascada
Conocer los 3 niveles de estilos (en línea, documento y externo)
Conocer los diferentes tipos de selectores (genéricos, de clase, id, universales y pseudo clases)
Revisar algunas de las propiedades de fuentes, listas y texto
Conocer el modelo de caja (The Box Model)
Aprender a separar la presentación del contenido en documentos HTML
 Instrucciones
Revisa la presentación CSS.
Crea un CSS externo y agrégalo al documento(s) que elaboraste en el laboratorio anterior.
El CSS debe contener al menos un estilo definido con cada uno de los selectores y el estilo debe ser aplicado al documento. Puedes utilizar como guía o o ayudarte de algunos de los sitios web que se enlistan en la sección de Recursos, o en algún otro sitio.
Asegúrate que tus documentos HTML no tengan estilos en línea.
Para mejor el rendimiento del sitio, crea una versión minimizada de tu CSS y enlázala a tu sitio en lugar del CSS que acabas de crear. Puedes apoyarte de herramientas como http://cssminifier.com/
Agrega las preguntas con sus respectivas respuestas en el documento HTML
 Preguntas a responder
Como ingeniero de software ¿cuál es tu recomendación sobre el uso de !important en un CSS? 
Si se pone una imagen de fondo en una página HTML, ¿por qué debe escogerse con cuidado?
Como ingeniero de software, ¿cuál es tu recomendación al elegir las unidades de un propiedad de estilo entre %, px y pt?
¿Por qué el uso de una versión minimizada del CSS mejora el rendimiento del sitio?
 Recursos
The box model

http://css-tricks.com/
http://css-tricks.com/examples/ButtonMaker/
http://cssglobe.com/
http://www.alistapart.com/topics/code/css/
http://sixrevisions.com/category/css/
Tutorial de SASS por Polo García
CSS Positioning Explained By Building An Ice Cream Sundae
CSS Selectors Explained By Going Car Shopping
Web Design in 4 minutes
The CSS Handbook: a handy guide to CSS for developers
The Ultimate Guide to CSS + Cheat Sheets
 Especificaciones de entrega
A través de tu repositorio personal (Bitbucket o GitHub)

