DAW y BD
Laboratorio 1: Introducción a las aplicaciones web, HTML5 y ciclo de vida de los sistemas de información
 Descripción
En esta actividad se abordarán brevemente los conceptos fundamentales para comenzar con el desarrollo de aplicaciones web, y se hará un breve repaso sobre html5.

 Modalidad
Individual.
 Objetivos de aprendizaje
Comprender los siguientes conceptos:
Internet
WWW
Navegadores web
URL
MME
HTTP (métodos y códigos de errores)
XML, JS, php, jsp, Ajax
Introducir HTML5
Revisar las principales etiquetas HTML (imágenes, hipervínculos, listas, tablas, formas)
Implementar interfaces de usuario HTML
Revisar editores HTML
Que conozcas el ciclo de vida y de desarrollo de los sistemas de información
Que desarrolles tu comepetencia de comunicación escrita
Que desarrolles tus competencias de búsqueda y análisis de información
Desarrollar tu competencia de aprendizaje por cuenta propia
 Instrucciones
Revisa los siguientes sitios de editores html:
Brackets
Sublime Text
Cloud 9
Codepen
Atom
NetBeans
Eclipse
Instala y/o configura el (los) de tu preferencia. Eres libre de utilizar cualquier otro que te agrade que no esté en la lista anterior, y de ser así, comarte el enlace en el grupo de slack del curso para que los demás lo conozcan.
Crea una página personal o un pequeño sitio donde muestres el uso de las principales etiquetas HTML. Puedes hablar de tus principales proyectos o aficiones, incluir artículos interesantes tuyos, sobre tí, o sobre algo que te apasione. Usa componentes semánticos como por ejemplo <header>, <footer> o <strong> en lugar de <div id="header">, <div id="footer"> y <b>. El sitio o página debe incluir alguna forma con controles.
No utilices etiquetas desaprobadas.
Recuerda incluir tus datos e información de contacto (nombre, matrícula, correo electrónico).
Utiliza tu creatividad.
No es necesario que la página cuente con estilos, ni JavaScript.
Pon el nombre del editor HMTL que utilizaste y el enlace al sitio del editor como pie de página.
Valida tus documentos con la herramienta validator.w3.org
Responde las preguntas de la siguiente sección en algún lugar del sitio.
En algún lugar del sitio HTML5 escribe también una breve descripción sobre: URL, MIME, métodos y códigos de errores HTTP, HTML5, JS, php, jsp, Ajax. Si no conoces alguna de estas tecnologías realiza una breve investigación, no te olvides de citar adecuadamente tus referencias.
Soporta  tu investigación utilizando libros, revistas o sitios WEB. En cualquier caso, cita las fuentes utilizadas. (utilizar al menos 2 fuentes de referencia distintos. Por ejemplo un libro y una sitio web). NO está permitido hacer un copiado de párrafos a menos que sea citadas adecuadamente.

·         Citar con el siguiente formato:

Para revista:      Autor, título del artículo, revista y año.

Para libro:           Autor, título del libro, páginas y año.

Para el sitio web: URL

Ejemplo de cómo citar:

Recientemente se ha acordado una clasificación de los algoritmos con base a la cantidad de memoria que ocupa en la computadora, dicha propuesta fue realizada en el año 2006 por Juan Pérez [1]

 …al final del documento

[1] Juan Pérez, Software Guru, 215 , 2006.

 

 Preguntas a responder
¿Cuál es la diferencia entre Internet y la World Wide Web?
¿Cuál es el propósito de los métodos HTTP: GET, HEAD, POST, PUT, PATCH, DELETE?
¿Qué método HTTP se debe utilizar al enviar un formulario HTML, por ejemplo cuando ingresas tu usuario y contraseña en algún sitio? ¿Por qué?
¿Qué método HTTP se utiliza cuando a través de un navegador web se accede a una página a través de un URL?
Un servidor web devuelve una respuesta HTTP con código 200. ¿Qué significa esto? ¿Ocurrió algún error?
¿Es responsabilidad del desarrollador corregir un sitio web si un usuario reporta que intentó acceder al sitio y se encontró con un error 404? ¿Por qué?
¿Es responsabilidad del desarrollador corregir un sitio web si un usuario reporta que intentó acceder al sitio y se encontró con un error 500? ¿Por qué?
¿Qué significa que un atributo HTML5 esté depreciado o desaprobado (deprecated)? Menciona algunos elementos de HTML 4 que en HTML5 estén desaprobados.
¿Cuáles son las diferencias principales entre HTML 4 y HTML5?
¿Qué componentes de estructura y estilo tiene una tabla?
¿Cuáles son los principales controles de una forma HTML5?
¿Qué tanto soporte HTML5 tiene el navegador que utilizas? Puedes utilizar la siguiente página para descubrirlo: http://html5test.com/ (Al responder la pregunta recuerda poner el navegador que utilizas)
Sobre el ciclo de vida y desarrollo de los sistemas de información:
¿Cuál es el ciclo de vida de los sistemas de información?
¿Cuál es el ciclo de desarrollo de sistemas de información?
 Recursos
Elementos semánticos de html5	Elementos semánticos de un artículo en html5
	
Ejemplo nuevos elementos en html5
The HTML Handbook
Beginner’s HTML Cheat Sheet
www.w3schools.com
www.w3.org/html/
validator.w3.org
www.html5rocks.com/es/
http://html5demos.com/
http://diveintohtml5.info/
http://html5.validator.nu
http://caniuse.com/
http://html5doctor.com/