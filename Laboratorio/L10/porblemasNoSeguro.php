<?php 
  include("_header.html");  
  include("_navBar.html");
  include("_pregunstasForm.html");



$celdas = 6 ;
print "<div>";

$arr = array();
for ($i = 0; $i < $celdas; $i++){
    $arr[$i] = $_POST['txt'.$i];
}
sort($arr);
$p = Promedio($arr);
$m = Mediana($arr);
print "<table class='table'>
<thead class='thead-dark'>
  <tr>
    <th>Promedio</th>
    <th>mediana</th>
  </tr>
</thead>
<tbody>
<tr>
  <td> $p </td>
  <td> $m</td>
</tr>
</tbody>
</table>";

listas($arr);
$limit = $_POST['number'];
tabla($limit);

$cuvo = new cube ($_POST['high'],$_POST['long'],$_POST['wide']);
$p1=$cuvo->pchl();
$p2=$cuvo->pchw();
$p3=$cuvo->pclw();

$a1=$cuvo->achl();
$a2=$cuvo->achw();
$a3=$cuvo->aclw();

$v=$cuvo->v();
print"
<div class='container mt-3'>
    <h2>Datos del cubo</h2>
    <p>Puedes filtrar para buscar un dato en especifico:</p>  
    <input class='form-control' id='myInput' type='text' placeholder='Search..'>
    <br>
    <ul class='list-group' id='myList'>
      <li class='list-group-item'>$p1 </li>
      <li class='list-group-item'>$p2 </li>
      <li class='list-group-item'>$p3 </li>
      <li class='list-group-item'>$a1 </li>
      <li class='list-group-item'>$a2 </li>
      <li class='list-group-item'>$a3 </li>
      <li class='list-group-item'>$v </li>

    </ul>  
  </div>
";

print "</div>";




//FUNCIONES
function Promedio($arr){
    $average = array_sum($arr)/count($arr);
    return $average;
}
function Mediana($arr){
    $cantidad = count($arr);
    $posicionMediana = intval(($cantidad + 1) / 2);
    $mediana = $arr [$posicionMediana];
    return $mediana;
}

function listas($arr){
    print ' 
    <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th class="table-dark">Posicion</th>
          <th class="table-secondary">1</th>
          <th class="table-secondary">2</th>
          <th class="table-secondary">3</th>
          <th class="table-secondary">4</th>
          <th class="table-secondary">5</th>
          <th class="table-secondary">6</th>
        </tr>
      </thead>
      <tbody>
';

        Ascendent($arr);
        Descendent($arr);   
    print "
    </tbody>
    </table>
    </div>
";
}
function Ascendent($arr){
    global $celdas;
    sort($arr);
    print "<tr>";
    print '<th class="table-dark">Ascendente</th>';
    for ($i = 0; $i < $celdas; $i++){
        print"<td>$arr[$i]</td>";
    }
    print "</td>";
}
function Descendent($arr){
    global $celdas;
    rsort($arr);
    print "<tr>";
    print '<th class="table-dark">Descendente</th>';
    for ($i = 0; $i < $celdas; $i++){
        print"<td>$arr[$i] </td>";
    }
    print "</tr>";
}

function tabla ($limit){
    print "
    <div class='container pt-3'>
    <div class='table-responsive anyClass'>
        <table class='table table-bordered'>
            <thead class='thead-dark'>
                <tr class= 'sticky'>
                    <th>Numero</th>
                    <th>Cuadrado</th>
                    <th>Cuvo</th>
                </tr>
            </thead>
    ";
    for($i = 1; $i <= $limit; $i++){
        print " <tr>";
        print "<td>".$i."</td>";
        print "<td>".pow($i,2)."</td>";
        print "<td>".pow($i,3)."</td>";
        print " </tr>";
    }
    print "</table>
    </div>
    </div>
    ";
}

//Class
class cube {
    public $high;
    public $long;
    public $wide;
  
    function __construct($high,$long,$wide) {
      $this->high = $high;
      $this->long = $long;
      $this->wide = $wide;
    }
    function pchl() {
    $x = 2*(($this->long) + ($this->high));
      return "<br>Perimetro de la cara de alto por largo:$x <br>" ; 
    }
    function pchw() {
        $x =2* (($this->wide) + ($this->high));
        return "<br>Perimetro de la cara de alto por ancho:$x <br>" ;
    }
    function pclw() {
        $x =2* (($this->wide) + ($this->long));
        return "<br>Perimetro de la cara de largo por ancho:$x <br>" ;
    }

    function achl() {
    $x = (($this->long) * ($this->high));
    return "<br>Area de la cara de alto por largo:$x <br>" ;
}
    function achw() {
        $x = (($this->wide) * ($this->high));
        return "<br>Area de la cara de alto por ancho:$x <br>" ;
    }
    function aclw() {
        $x = (($this->wide) * ($this->long));
        return "<br>Area de la cara de largo por ancho:$x <br>" ;
    }
    function v() {
        $x = (($this->wide) * ($this->long)* ($this->high));
        return "<br>Volumen:$x <br>" ;
    }
  }

  include("_endPage.html"); 

?> 