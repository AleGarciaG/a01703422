
DROP TABLE IF EXISTS Materiales;
DROP TABLE IF EXISTS Proyectos;
DROP TABLE IF EXISTS Proveedores;
DROP TABLE IF EXISTS Entregan;

CREATE TABLE Materiales 
( 
  Clave numeric(5)  , 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) ;

CREATE TABLE Proyectos 
( 
  Numero numeric(5)  , 
  Denominacion varchar(50) 
) ;



CREATE TABLE Proveedores 
( 
  RFC char(13)  , 
  RazonSocial varchar(50) 
) ;



CREATE TABLE Entregan 
( 
  Clave numeric(5)  , 
  RFC char(13)  , 
  Numero numeric(5)  , 
  Fecha DateTime  , 
  Cantidad numeric (8,2) 
) ;
