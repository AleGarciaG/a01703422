<?php
function connectDb($servername = "localhost",$username = "root",$password = "",$dbname = "Lab16"){
    $con = mysqli_connect($servername, $username, $password, $dbname);

    //Check connection
    if(!$con)
        die("Connection failed: " . mysqli_connect_error());
    
    return $con;
}
function closeDb($mysql){
    mysqli_close($mysql);
}
function getFruits($pais="", $fruta="", $ordenarby=""){
    $conn = connectDb();
    $sql = "SELECT I.id as I_id, F.name as F_name, I.units as I_units, I.quantity as I_quantity, I.price as I_price, P.name as P_name FROM Producto as I, Paises as P, Fruta as F WHERE I.idPais = P.id AND I.idFruta = F.id "  ;
    if ($pais != "") {
        //$sql .= " AND P.id=".$pais;
        $sql .= " AND P.id=".$pais;
    }
    if ($fruta != "") {
        $sql .= " AND I.idFruta =".$fruta;
    }
    if ($ordenarby != "") {
        switch($ordenarby){
            case '0':{
                $sql .= " ORDER BY I_id ASC";
            break;
            }
            case  '1':{
                $sql .= " ORDER BY F_name ASC";
            break;
            }
            case  '2':{
                $sql .= " ORDER BY I_units ASC";
            break;
            }
            case  '3':{
                $sql .= " ORDER BY I_quantity ASC";
            break;
            }
            case  '4':{
                $sql .= " ORDER BY I_price ASC";
            break;
            }
            case '5':{
                $sql .= " ORDER BY P_name ASC";
            break;
            }

        }

    }
    $result = mysqli_query($conn, $sql);
    closeDb($conn);
    return $result;
}

function crear_select($id, $columna_descripcion, $tabla,$name,$defaultId=0,$disabled=0) {
    $conn = connectDb();  
      
    $result = "<div class='form-group'>
                    <label for='$tabla'>$tabla:</label>
                    <select class='form-control' name='".$tabla."S$name' id id='".$tabla."S$name'
                    
                    ";
                    if($disabled){
    $result .=  "disabled";
                    }
                    
    $result .=                 ">
                    <option value='' disabled selected> Selecciona una opción</option>";
            
    $sql = "SELECT $id, $columna_descripcion FROM $tabla";
    $datos = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($datos, MYSQLI_BOTH)) {
        $result .= '<option value="'.$row["$id"].'"';

        if ($defaultId  == $row["$columna_descripcion"]){
            $result .= 'selected';
        }
        $result .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    closeDb($conn);
    $result .=  '</select></div>';
    return $result;
  }


  function crear_selectG($id, $columna_descripcion, $tabla) {
    $conn = connectDb();  
    $result = "
    <div class='input-group-prepend''>
                    <select class='form-control' name='".$tabla."G' id id='".$tabla."G'>
                    <option value='' disabled selected> Selecciona una opción</option>";
            
    $sql = "SELECT $id, $columna_descripcion FROM $tabla";
    $datos = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($datos, MYSQLI_BOTH)) {
        $result .= '<option value="'.$row["$id"].'">'.$row["$columna_descripcion"].'</option>';
    }
        
    closeDb($conn);
    $result .=  '</select></div>';
    return $result;
  }

  function insertar_caso_f($fruta_name) {
    $conn = connectDb();
    //Prepara la consulta
    $dml = 'INSERT INTO fruta (name) VALUES (?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_clean = cleamData ($fruta_name);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $dat_clean)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  //
  function insertar_caso_p($paises_name) {
    $conn = connectDb();
    //Prepara la consulta
    $dml = 'INSERT INTO paises (name) VALUES (?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_clean = cleamData ($paises_name);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $dat_clean)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  function insertar_caso_pro($idFruta,$units,$quantity,$price,$idPais) {
    $conn = connectDb();
    //Prepara la consulta

    $dml = 'INSERT INTO Producto (idFruta,units,quantity,price,idPais) VALUES (?,?,?,?,?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_cleanF = cleamData ($idFruta);
    $dat_cleanU = cleamData ($units);
    $dat_cleanQ = cleamData ($quantity);
    $dat_cleanP = cleamData ($price);
    $dat_cleanPA = cleamData ($idPais);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("sssss", $dat_cleanF, $dat_cleanU, $dat_cleanQ, $dat_cleanP, $dat_cleanPA)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  //
  function cleamData ($dataToCleam){
    return stripslashes(trim(htmlspecialchars($dataToCleam)));
  }

  function delate_caso_f($fruta_id) {
    $conn = connectDb();
    //Prepara la consulta
     $dml = 'DELETE FROM Fruta WHERE id = (?)';

    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_clean = cleamData ($fruta_id);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $dat_clean)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }

  function delate_caso_p($pais_id) {
    $conn = connectDb();
    //Prepara la consulta
     $dml = 'DELETE FROM Paises WHERE id = (?)';

    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_clean = cleamData ($pais_id);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $dat_clean)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  function delate_caso_pro($producto_id) {
    $conn = connectDb();
    //Prepara la consulta
     $dml = 'DELETE FROM Producto WHERE id = (?)';

    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_clean = cleamData ($producto_id);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $dat_clean)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }

  function modificar_caso_f($idFruta,$fruta_name) {
    $conn = connectDb();
    //Prepara la consulta
    $dml = 'UPDATE Fruta SET name=(?) WHERE id=(?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_cleanid = cleamData ($idFruta);
    $dat_cleanname = cleamData ($fruta_name);

    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $dat_cleanname,$dat_cleanid)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  function modificar_caso_p($idPais,$pais_name) {
    $conn = connectDb();
    //Prepara la consulta
    $dml = 'UPDATE Paises SET name=(?) WHERE id=(?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_cleanid = cleamData ($idPais);
    $dat_cleanname = cleamData ($pais_name);

    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $dat_cleanname,$dat_cleanid)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  function modificar_caso_pro($id,$idFruta,$units,$quantity,$price,$idPai) {
    $conn = connectDb();
    //Prepara la consulta
    $dml = 'UPDATE Producto SET idFruta=(?), units=(?),quantity=(?),price=(?),idPais=(?) WHERE id=(?)';
    if ( !($statement = $conn->prepare($dml)) ) {
        die("Error: (" . $conn->errno . ") " . $conn->error);
        return 0;
    }
    //cleam input
    $dat_cleanid = cleamData ($id);
    $dat_cleanidFruta = cleamData ($idFruta);
    $dat_cleanunits = cleamData ($units);
    $dat_cleanquantity = cleamData ($quantity);    
    $dat_cleanprice = cleamData ($price);
    $dat_cleanidPai = cleamData ($idPai);
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ssssss", $dat_cleanidFruta,$dat_cleanunits,$dat_cleanquantity,$dat_cleanprice,$dat_cleanidPai,$dat_cleanid)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    closeDb($conn);
      return 1;
  }
  

?>