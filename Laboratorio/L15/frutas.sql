-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2020 a las 02:39:45
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Base de datos: `Lab15`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Producto`
--
DROP TABLE IF EXISTS Producto;

CREATE TABLE `Producto` (
  `id` int(11) NOT NULL,
  `idFruta` int(11) NOT NULL,
  `units` int(11) ,
  `quantity` int(11) ,
  `price` int(11) ,
  `idPais` int(11) NOT NULL


) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Producto`
--

INSERT INTO `Producto` (`id`,`idFruta`, `units`, `quantity`,`price` ,`idPais`) VALUES
(0,0, 201, 1,2000 ,0),
(1,1, 101, 5,50 ,1),
(2,1, 1560, 3651,100 ,2),
(3,1, 10, 1356,13500 ,3),
(4,2, 13650, 51,136500 ,0),
(5,2, 10, 1,10630 ,1),
(6,2, 15360, 61,100 ,2),
(7,2, 16530, 3651,103650 ,3),
(8,3, 15360, 3651,100 ,2),
(9,3, 153560, 3651,12200 ,4),
(10,4, 13560, 51,136500 ,2);

-- Estructura de tabla para la tabla `paises`
--
DROP TABLE IF EXISTS paises;

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `name` varchar(40)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--
INSERT INTO `paises` (`id`,`name`) VALUES
(0,'Japon'),
(1,'China'),
(2,'Mexico'),
(3,'Alemania'),
(4,'Rusia'),
(5,'Peru');

DROP TABLE IF EXISTS fruta;

CREATE TABLE `fruta` (
  `id` int(11) NOT NULL,
  `name` varchar(40)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--
  INSERT INTO `fruta` (`id`,`name`) VALUES
  (0,'guayaba'),
  (1,'manzana'),
  (2,'pera'),
  (3,'platano'),
  (4,'pina');


-- Indices de la tabla Producto
ALTER TABLE `Producto` 
  ADD PRIMARY KEY (`id`),
  ADD  KEY `idPais` (`idPais`),
  ADD  KEY `idFruta` (`idFruta`)

  ;
-- Indices de la tabla paises
ALTER TABLE `paises` 
  ADD PRIMARY KEY (`id`);
-- Indices de la tabla fruta
ALTER TABLE `fruta` 
  ADD PRIMARY KEY (`id`);



-- AUTO_INCREMENT de la tabla `Producto`
ALTER TABLE `Producto`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
-- AUTO_INCREMENT de la tabla `Producto`
ALTER TABLE `paises`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `fruta`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;


-- Filtros para la tabla `Producto`
--
ALTER TABLE `Producto`
  ADD CONSTRAINT `tiene_pais` FOREIGN KEY (`idPais`) REFERENCES `paises` (`id`),
  ADD CONSTRAINT `tiene_nombre` FOREIGN KEY (`idFruta`) REFERENCES `fruta` (`id`);

SET FOREIGN_KEY_CHECKS=1;


  
COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;