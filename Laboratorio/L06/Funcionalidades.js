function allowDrop(ev) {
    ev.preventDefault();
  }
  
  function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
    tabla ();
  }
  function serif(){
    document.getElementById("mynombre").style.fontFamily = '"Times New Roman", Times, serif';
  }
  function sans(){
    document.getElementById("mynombre").style.fontFamily = 'Arial, Helvetica, sans-serif';
  }
  function monospace(){
    document.getElementById("mynombre").style.fontFamily = '"Lucida Console", Console, monospace';
  }