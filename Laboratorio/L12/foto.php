<?php 
  session_start();
?> 
<?php 
  
  include("_header.html");  
  include("_navBar.html");
  print("
  <div class='container mt-3 containerImage'>
  <div id='demo' class='carousel slide' data-ride='carousel' style=' width:100%; height: 500px' >

    <!-- Indicators -->
    <ul class='carousel-indicators'>
      <li data-target='#demo' data-slide-to='0' class='active'></li>
      <li data-target='#demo' data-slide-to='1'></li>
      <li data-target='#demo' data-slide-to='2'></li>
    </ul>
    
    <!-- The slideshow -->
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img src='https://www.recursosdeautoayuda.com/wp-content/uploads/2013/05/steve-jobs.png' alt='steve' width='1100' height='500'>
      </div>
      <div class='carousel-item'>
        <img src='https://2.bp.blogspot.com/-EBTUMXr12OY/VvBvvi_VLnI/AAAAAAAASPA/s1sJVdkgDwAAZurY-6oHKD80WZdtcuGlA/s640/Captura%2Bde%2Bpantalla%2B2016-03-21%2Ba%2Blas%2B23.03.20.png' alt='teresa' width='1100' height='500'>
      </div>
      <div class='carousel-item'>
        <img src='https://images2.revistafama.com/IA8Dg-ie8jS438nl4IQ797f0dyk=/958x596/uploads/media/2019/11/11/leonardo-dicaprio_0_7_780_485.png' alt='Leonardo' width='1100' height='500'>
      </div>
    </div>
    
    <!-- Left and right controls -->    
    <a class='carousel-control-prev' href='#demo' data-slide='prev'>
      <span class='carousel-control-prev-icon'></span>
    </a>
    <a class='carousel-control-next' href='#demo' data-slide='next'>
      <span class='carousel-control-next-icon'></span>
    </a>
  </div>
  </div>
  ");

  include("_fotosDisplay.html");
  include("_endPage.html"); 
?> 