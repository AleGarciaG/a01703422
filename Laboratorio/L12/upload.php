<?php 
  session_start();
?> 
<?php
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    include("_header.html");  
    include("_navBar.html");
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        $filepath = "uploads/" . $_FILES["fileToUpload"]["name"];
        if(($check !== false ) && (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $filepath)) ){
            print"
            <div class='alert alert-success alert-dismissible fade show'>
            <button type='button' class='close' data-dismiss='alert'>&times;</button>
            <strong>Success!</strong> Podras ver tu foto en el cuadro de la fama.
          </div>
            ";
            $uploadOk = 1;     
        } else {
            print"
            <div class='alert alert-danger alert-dismissible fade show'>
            <button type='button' class='close' data-dismiss='alert'>&times;</button>
            <strong>Error!</strong> No es una foto o un formato aceptable.
          </div>
            ";
            $uploadOk = 0;
        }
    }

  print("
  <div class='container mt-3 containerImage text-center jumbotron'>
  <h3> Cuadro de la fama</h3>
  <div id='demo' class='carousel slide' data-ride='carousel' style=' width:100%; height: 500px' >

    <!-- Indicators -->
    <ul class='carousel-indicators'>
      <li data-target='#demo' data-slide-to='0' class='active'></li>
      <li data-target='#demo' data-slide-to='1'></li>
      <li data-target='#demo' data-slide-to='2'></li>
   ");
    if ($uploadOk){
        print("<li data-target='#demo' data-slide-to='3'></li> ");
    }
   print("   
    </ul>
    
    <!-- The slideshow -->
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img src='https://www.recursosdeautoayuda.com/wp-content/uploads/2013/05/steve-jobs.png' alt='steve' width='1100' height='500'>
      </div>
      <div class='carousel-item'>
        <img src='https://2.bp.blogspot.com/-EBTUMXr12OY/VvBvvi_VLnI/AAAAAAAASPA/s1sJVdkgDwAAZurY-6oHKD80WZdtcuGlA/s640/Captura%2Bde%2Bpantalla%2B2016-03-21%2Ba%2Blas%2B23.03.20.png' alt='teresa' width='1100' height='500'>
      </div>
      <div class='carousel-item'>
        <img src='https://images2.revistafama.com/IA8Dg-ie8jS438nl4IQ797f0dyk=/958x596/uploads/media/2019/11/11/leonardo-dicaprio_0_7_780_485.png' alt='Leonardo' width='1100' height='500'>
      </div>
    ");
    if ($uploadOk){
        print("
        <div class='carousel-item'>
        <img src='$filepath' alt='TuFoto' width='1100' height='500'>
        </div>
        ");
    }
    print("
    </div>
    
    <!-- Left and right controls -->    
    <a class='carousel-control-prev' href='#demo' data-slide='prev'>
      <span class='carousel-control-prev-icon'></span>
    </a>
    <a class='carousel-control-next' href='#demo' data-slide='next'>
      <span class='carousel-control-next-icon'></span>
    </a>
  </div>
  </div>
  ");

  include("_fotosDisplay.html");
  include("_endPage.html"); 
?> 


