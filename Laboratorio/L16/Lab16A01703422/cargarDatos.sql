LOAD DATA INFILE 'materiales.csv' 
INTO TABLE lab16DB.materiales
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'proveedores.csv' 
INTO TABLE lab16db.proveedores
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'proyectos.csv' 
INTO TABLE lab16db.proyectos
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'entregan.csv' 
INTO TABLE lab16DB.entregan
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(Clave,RFC,Numero,@Fecha,Cantidad)
SET Fecha = STR_TO_DATE(@Fecha, '%d/%m/%Y');