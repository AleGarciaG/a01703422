<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EditarEscolaridad/_editarEscolaridadTitulo.html");
    include("Partials/EditarEscolaridad/_editarEscolaridadFormularioHead.html");
        $escolaridad_id = htmlspecialchars($_GET["escolaridad_id"]);
        $beneficiaria_id = htmlspecialchars($_GET["beneficiaria_id"]);
        $escuela_id = htmlspecialchars($_GET["escuela_id"]);
        $gradoEscolar_id = htmlspecialchars($_GET["gradoEscolar_id"]);

        
        echo"
            <form action=\"Controladores\Escolaridad\controladorEditarEscolaridad.php?escolaridad_id=$escolaridad_id\" method=\"post\">
                <h2>Beneficiaria</h2>
                <div class=\"file-field input-field\">
                <div class=\"input-field col s12\">
                    <i class=\"material-icons prefix\"> </i>
        ";
                echo "".crear_selectBeneficiaria($beneficiaria_id)."";
        echo"       </div>
                </div>    
                <h2>Escolaridad</h2>
                <div class=\"file-field input-field\">
                    <div class=\"input-field col s6\">
                    <i class=\"material-icons prefix\"> </i>
        ";
                echo "".crear_select("idEscuela", "nombre", "Escuela",$escuela_id)."";
        echo"
                </div>
                    <div class=\"input-field col s6\">
                    <i class=\"material-icons prefix\"> </i>
        ";
                echo "".crear_select("idGradoEscolar", "nombre", "GradoEscolar",$gradoEscolar_id)."";
        echo"</div>
        </div>
      </div>
      <!--Elemento-->
      <div class=\"file-field input-field\">
        <div class=\"input-field col s6\">
          
        ".
        fechaCamp("idEscolaridad","Escolaridad",$escolaridad_id,"fechaInicio")
        ."    
        </div>
        <div class=\"input-field col s6\">
          
".
fechaCamp("idEscolaridad","Escolaridad",$escolaridad_id,"fechaFin")
."   
        </div>
      </div>
  </div>

  <div class=\"carousel-item teal lighten-5\" href=\"#two!\">
    <h2>Datos tutor</h2>
    <div class=\"col s12\">
        <!-- Elemento -->
".
textCamp("idEscolaridad","Escolaridad",$escolaridad_id,"nombreTutor")
."
        <!-- Elemento -->
        <div class=\"file-field input-field\">
          <div class=\"input-field col s6\">
".
emailCamp("idEscolaridad","Escolaridad",$escolaridad_id,"correoElectronico")
."  
          </div>
          <div class=\"input-field col s6\">
".
telefonoCamp("idEscolaridad","Escolaridad",$escolaridad_id,"telefono")

."
          </div>  
        </div>

        ";
        include("Partials/EditarEscolaridad/_editarEscolaridadFormularioFoot.html");
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>  