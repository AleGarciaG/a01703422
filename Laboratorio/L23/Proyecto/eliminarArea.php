<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EliminarArea/_eliminarAreaTitulo.html");
    $area_id = htmlspecialchars($_GET["area_id"]);
        echo getAreaById($area_id);
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>
