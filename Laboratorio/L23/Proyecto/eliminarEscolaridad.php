<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EliminarEscolaridad/_eliminarEscolaridadTitulo.html");
    $escolaridad_id = htmlspecialchars($_GET["escolaridad_id"]);
        echo getEscolaridadByIdE($escolaridad_id);
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>
