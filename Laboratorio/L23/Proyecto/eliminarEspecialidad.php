<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EliminarEspecialidad/_eliminarEspecialidadTitulo.html");
    $especialidad_id = htmlspecialchars($_GET["especialidad_id"]);
        echo getEspecialidadById($especialidad_id);
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>
