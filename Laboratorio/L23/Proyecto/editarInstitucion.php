<?php
    session_start();
    include("util.php");
    $id = htmlspecialchars($_GET["institucion_id"]);
    $_SESSION["idI"] = $_GET["institucion_id"];
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultarInstitucion/_consultarInstitucionTitulo.html"); 
    echo editInstitucion($id);
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>