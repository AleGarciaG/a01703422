<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEscuela/_escuelasTitulo.html");
    include("Partials/ConsultaEscuela/_fedback.html");

    echo "<div class=\"row\">";
        echo "<div class=\"col s12\">";
        include("Partials/ConsultaEscuela/_consultaEscuelaHead.html");
        include("Partials/ConsultaEscuela/_consultaEscuela.html");    //cambio, para hacer nuestra tabla de consulta de programas dinamica debemos partir en 2 partials este archivo
        $escuelas = "";
        
        echo getEscuelas($escuelas);
        
        include("Partials/ConsultaEscuela/_consultaEscuelaFoot.html");
        echo "</div>";
    echo "</div>";


    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>