<?php
  session_start();
  require_once("../../util.php");  

  $_POST["area"] = htmlspecialchars($_POST["area"]);
  $_POST["especialidad_nombre"] = htmlspecialchars($_POST["especialidad_nombre"]);

  if(isset($_POST["especialidad_nombre"])) {
      if (insertarEspecialidad($_POST["especialidad_nombre"], $_POST["area"])) {
          $_SESSION["mensaje"] = "Se agrego una nueva especialidad";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al agregar una nueva  especialidad";
      }
  }

  header("location:../../consultaEspecialidad.php");
?>