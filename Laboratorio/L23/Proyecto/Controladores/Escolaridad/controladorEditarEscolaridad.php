<?php
  session_start();
  require_once("../../util.php");  

  $_GET["escolaridad_id"] = htmlspecialchars($_GET["escolaridad_id"]);
  $_POST["beneficiaria"] = htmlspecialchars($_POST["beneficiaria"]);
  $_POST["Escuela"] = htmlspecialchars($_POST["Escuela"]);
  $_POST["GradoEscolar"] = htmlspecialchars($_POST["GradoEscolar"]);
  $_POST["Escolaridad_fechaInicio"] = htmlspecialchars($_POST["Escolaridad_fechaInicio"]);
  $_POST["Escolaridad_fechaFin"] = htmlspecialchars($_POST["Escolaridad_fechaFin"]);

  $_POST["Escolaridad_nombreTutor"] = htmlspecialchars($_POST["Escolaridad_nombreTutor"]);
  $_POST["Escolaridad_correoElectronico"] = htmlspecialchars($_POST["Escolaridad_correoElectronico"]);
  $_POST["Escolaridad_telefono"] = htmlspecialchars($_POST["Escolaridad_telefono"]);

  if(isset($_POST["beneficiaria"],$_POST["Escuela"],$_POST["GradoEscolar"])) {
      if (editarEscolaridad($_GET["escolaridad_id"],$_POST["beneficiaria"],$_POST["Escuela"],$_POST["GradoEscolar"],
      $_POST["Escolaridad_nombreTutor"],$_POST["Escolaridad_telefono"],$_POST["Escolaridad_correoElectronico"],
      $_POST["Escolaridad_fechaInicio"],$_POST["Escolaridad_fechaFin"])) {
          $_SESSION["mensaje"] = "Se edito una escolaridad";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar la escolaridad";
      }
  }

  header("location:../../consultaEscolaridad.php");
?>