<?php
  session_start();
  require_once("../../util.php");  

  $_POST["area_nombre"] = htmlspecialchars($_POST["area_nombre"]);

  if(isset($_POST["area_nombre"])) {
      if (insertarArea($_POST["area_nombre"])) {
          $_SESSION["mensaje"] = "Se agrego una nueva area";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al agregar una nueva  area";
      }
  }

  header("location:../../consultaArea.php");
?>