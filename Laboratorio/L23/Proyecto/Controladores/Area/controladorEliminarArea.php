<?php
  session_start();
  require_once("../../util.php");  

  $_GET["area_id"] = htmlspecialchars($_GET["area_id"]);

  if(isset($_GET["area_id"])) {
      if (eliminarArea($_GET["area_id"])) {
          $_SESSION["mensaje"] = "Se elimino la area";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al eliminar la area";
      }
  }

  header("location:../../consultaArea.php");
?>