<?php
  session_start();
  require_once("../../util.php");  

  $_GET["gradoEscolar_id"] = htmlspecialchars($_GET["gradoEscolar_id"]);

  if(isset($_GET["gradoEscolar_id"])) {
      if (eliminarGradoEscolar($_GET["gradoEscolar_id"])) {
          $_SESSION["mensaje"] = "Se elimino el grado escolar";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al eliminar el grado escolar";
      }
  }

  header("location:../../consultaGradoEscolar.php");
?>