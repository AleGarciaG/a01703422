<?php
 session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
   
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEscolaridad/_fedback.html");

    if(isset($_POST['Actualizar'])){
        $id=$_SESSION["id"];
        $nombre = htmlspecialchars($_POST["nombre"]);
        $ingrediente = htmlspecialchars($_POST['ingrediente']);
        $presentacion = htmlspecialchars($_POST['presentacion']);
        if (modificarMedicamento($id,$nombre,$ingrediente,$presentacion)) {
            $_SESSION["mensaje"] = "Se ha editado correctamente";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al modificar";
        }

       // modificarMedicamento($id,$nombre,$ingrediente,$presentacion);
        include("Partials/ConsultaMedicamento/_consultaMedicamentoTitulo.html");

        include("Partials/ConsultaMedicamento/_consultaMedicamento.html"); 
        showQueryMedicamentos(getMedicamentos());
    }
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>