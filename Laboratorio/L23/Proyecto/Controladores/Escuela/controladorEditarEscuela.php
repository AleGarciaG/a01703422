<?php
  session_start();
  require_once("../../util.php");  

  $_GET["escuela_id"] = htmlspecialchars($_GET["escuela_id"]);
  $_POST["escuela_nombre"] = htmlspecialchars($_POST["escuela_nombre"]);

  if(isset($_GET["escuela_id"]) && isset($_POST["escuela_nombre"])) {
      if (editarEscuela($_GET["escuela_id"], $_POST["escuela_nombre"])) {
          $_SESSION["mensaje"] = "Se edito la escuela";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar la escuela";
      }
  }

  header("location:../../consultaEscuela.php");
?>