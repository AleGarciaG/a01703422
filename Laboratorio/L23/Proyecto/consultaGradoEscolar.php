<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaGradoEscolar/_gradoEscolarTitulo.html");
    include("Partials/ConsultaGradoEscolar/_fedback.html");

    echo "<div class=\"row\">";
        echo "<div class=\"col s12\">";
        include("Partials/ConsultaGradoEscolar/_consultaGradoEscolarHead.html");
        include("Partials/ConsultaGradoEscolar/_consultaGradoEscolar.html");    //cambio, para hacer nuestra tabla de consulta de programas dinamica debemos partir en 2 partials este archivo
        $gradoEscolar = "";
        
        echo getGradoEscolar($gradoEscolar);
        
        include("Partials/ConsultaGradoEscolar/_consultaGradoEscolarFoot.html");
        echo "</div>";
    echo "</div>";


    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>