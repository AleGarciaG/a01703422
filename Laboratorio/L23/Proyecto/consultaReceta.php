<?php
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaReceta/_consultaRecetaTitulo.html");
        echo "<div class=\"row\">";
            echo "<div class=\"col s4\">";
            include("Partials/ConsultaReceta/_consultaReceta.html");
            echo "</div>";
            if(isset($_POST['receta'])){
                echo "<div class=\"col s8\">";
                include("Partials/ConsultaReceta/_receta.html");
                echo "</div>";
            }
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>