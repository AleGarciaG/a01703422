<?php
    session_start();
    include("util.php");
    $id = htmlspecialchars($_GET["programa_id"]);
    $_SESSION["idP"] = $_GET["programa_id"];
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultarPrograma/_consultarProgramaTitulo.html"); 
    echo AreasEdit($id);
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>