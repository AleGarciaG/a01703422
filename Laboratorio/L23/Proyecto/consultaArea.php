<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaArea/_areaTitulo.html");
    include("Partials/ConsultaArea/_fedback.html");

        echo "<div class=\"row\">";
            echo "<div class=\"col s12\">";
                include("Partials/ConsultaArea/_consultaAreaHead.html");
                include("Partials/ConsultaArea/_consultaArea.html");    
                $Areas = "";   
                
                echo getAreas($Areas);
                
                include("Partials/ConsultaArea/_consultaAreaFoot.html");
            echo "</div>";
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>