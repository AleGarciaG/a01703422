<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EliminarEscuela/_eliminarEscuelaTitulo.html");
    $escuela_id = htmlspecialchars($_GET["escuela_id"]);
        echo getEscuelaById($escuela_id);
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>
