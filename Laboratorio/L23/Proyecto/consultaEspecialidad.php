<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEspecialidad/_especialidadTitulo.html");
    include("Partials/ConsultaEspecialidad/_fedback.html");

        echo "<div class=\"row\">";
            echo "<div class=\"col s12\">";
                include("Partials/ConsultaEspecialidad/_consultaEspecialidadHead.html");
                include("Partials/ConsultaEspecialidad/_consultaEspecialidad.html");    
                $especialidads = "";   
                
                echo getEspecialidades($especialidads);
                
                include("Partials/ConsultaEspecialidad/_consultaEspecialidadFoot.html");
            echo "</div>";
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>