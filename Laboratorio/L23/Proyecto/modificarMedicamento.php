<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
   
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ModificarMedicamento/_modificarMedicamentoTitulo.html");
    include("Partials/ModificarMedicamento/_modificarMedicamento.html");

    $_SESSION["id"] = htmlspecialchars($_GET["medicamento_id"]);
    if(isset($_POST['Actualizar'])){
        $id=htmlspecialchars($_SESSION["id"]);
        $nombre = htmlspecialchars($_POST["nombre"]);
        $ingrediente = htmlspecialchars($_POST['ingrediente']);
        $presentacion = htmlspecialchars($_POST['presentacion']);

        modificarMedicamento($id,$nombre,$ingrediente,$presentacion);
        echo "<div class=\"card-content white-text blue lighten-2\">
        <span class=\"card-title\">Cambio Exitoso</span>
        </div>";
    }
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>