<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
   
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/AltaMedicamento/_altaMedicamentoTitulo.html");
    include("Partials/AltaMedicamento/_altaMedicamentoFormulario.html");
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>