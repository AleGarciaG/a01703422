<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    
    include("Partials/ConsultaDiscapacidad/_discapacidadesTitulo.html");
    include("Partials/ConsultaDiscapacidad/_fedback.html");

    echo "<div class=\"row\">";
        echo "<div class=\"col s12\">";
        include("Partials/ConsultaDiscapacidad/_consultaDiscapacidadHead.html");
        include("Partials/ConsultaDiscapacidad/_consultaDiscapacidad.html");    //cambio, para hacer nuestra tabla de consulta de programas dinamica debemos partir en 2 partials este archivo
        $Discapacidades = "";   
        
        echo getDiscapacidades($Discapacidades);
        
        include("Partials/ConsultaDiscapacidad/_consultaDiscapacidadFoot.html");
        echo "</div>";
    echo "</div>";


    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>