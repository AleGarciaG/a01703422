<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");

    include("Partials/AltaEscolaridad/_altaEscolaridadTitulo.html");
    include("Partials/AltaEscolaridad/_altaEscolaridadFormulario.html");
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>