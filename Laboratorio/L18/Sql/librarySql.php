<?php
    function conectar_bd() {

        $conexion_bd = mysqli_connect("localhost","root","","needed");

        if ($conexion_bd == NULL) {

            die("No se pudo conectar con la base de datos");

        }

        return $conexion_bd;
    }

//función para desconectarse de una bd

    function desconectar_bd($conexion_bd) {

        mysqli_close($conexion_bd);

    }

    function cleamData ($dataToCleam){
        return stripslashes(trim(htmlspecialchars($dataToCleam)));
    }

    function insertar_caso_p($paises_name) {
        $conn = conectar_bd();
        //Prepara la consulta
        $dml = 'INSERT INTO paises (name) VALUES (?)';
        if ( !($statement = $conn->prepare($dml)) ) {
            die("Error: (" . $conn->errno . ") " . $conn->error);
            return 0;
        }
        //cleam input
        $dat_clean = cleamData ($paises_name);
        //Unir los parámetros de la función con los parámetros de la consulta   
        //El primer argumento de bind_param es el formato de cada parámetro
        if (!$statement->bind_param("s", $dat_clean)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
          
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
    
        desconectar_bd($conn);
        return 1;
    }
    function getProgramas(){
        $conn = conectar_bd();
        $sql = "SELECT P.idProgramaAtencion as P_id,  A.nombre as A_nombre, P.nombreProgramaAtencion as P_nombre,DATE_FORMAT(P.fechaInicial, '%d/%m/%Y') as P_fechaInicio FROM ProgramaAtencion as P, Area as A WHERE P.idArea = A.idArea";
        $result = mysqli_query($conn, $sql);
        $table = "
        <form id='programa'>
        <table class='table'>
        <thead>
            <tr>
            <th>Ver</th>
            <th>Area</th>
            <th>Nombre</th>
            <th>Fecha Inicio</th>
            </tr>
        </thead>
        <tbody id='myTable'>
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $table.= "<tr>";
                $table.= "<td><button  type='button' name='receta' class='btn' onclick='mostrarPrograma(". $row["P_id"]. ")'/>Programa</button></td> ";
                $table.= "<td>". $row["A_nombre"]. "</td>";
                $table.= "<td>". $row["P_nombre"]. "</td>";
                $table.= "<td>". $row["P_fechaInicio"]. "</td>";
                $table.= "</tr>";
            }
            $table.= "        
                </tbody>
            </table>
        </form>";
        desconectar_bd($conn);
        return $table;
    }
    function getPrograma($idPrograma){
        $conn = conectar_bd();
        $sql = "SELECT A.nombre as A_nombre,P.objetivo as P_objetivo ,P.nombreProgramaAtencion as P_nombre,DATE_FORMAT(P.fechaInicial, '%d/%m/%Y') as P_fechaInicio,DATE_FORMAT(P.fechaFinal, '%d/%m/%Y') as P_fechaFinal FROM ProgramaAtencion as P, Area as A WHERE P.idArea = A.idArea  and $idPrograma=P.idProgramaAtencion";
        $result = mysqli_query($conn, $sql);
        $resultado = "
        <div class='card blue-grey darken-1'>

        <div class='card-content white-text'>
     
           <span class='card-title'>Programa:
        ";
            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){   
                $resultado.= $row["P_nombre"]. "<br>Fecha de inicio:";
                $resultado.= $row["P_fechaInicio"]. "</span>
                <p>Descripción:
                ";
                $resultado.= $row["P_objetivo"]. "</p>
                <p>Fecha final de programa:
                ";
                $resultado.= $row["P_fechaFinal"]."</p>
                    </div>
                    <div class='card-action'>

                    <a href='#'>Consultar Programa</a>
            
                </div>
            
                </div>
                
                ";
            }
        desconectar_bd($conn);
        return $resultado;
    }




?>