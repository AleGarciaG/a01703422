<?php
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaMedicamento/_consultaMedicamentoTitulo.html");
        echo "<div class=\"row\">";
            echo "<div class=\"col-4\">";
            include("Partials/ConsultaMedicamento/_consultaMedicamento.html");
            echo "</div>";
            if(isset($_POST['medicamento'])){
                echo "<div class=\"col-8\">";
                include("Partials/ConsultaMedicamento/_medicamento.html");
                echo "</div>";
            }
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>