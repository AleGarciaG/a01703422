SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS  Lugares;
DROP TABLE IF EXISTS  IncidenteTipo;
DROP TABLE IF EXISTS  IncidenteSeguridad;


CREATE TABLE `Lugares`
(
	`id`        int(11) not null AUTO_INCREMENT,
	`nombre`    varchar(40),
    PRIMARY KEY (`id`)
);
CREATE TABLE `IncidenteTipo`
(
	`id`        int(11) not null AUTO_INCREMENT,
	`nombre`    varchar(40),
    PRIMARY KEY (`id`)
);
CREATE TABLE `IncidenteSeguridad`
(
	`idLugares`         int(11)         not null,
    `idIncidenteTipo`   int(11)         not null,
    `fecha`             TIMESTAMP       not null DEFAULT NOW(),
    PRIMARY KEY (`idLugares`,`idIncidenteTipo`,`fecha`),
    FOREIGN KEY (`idLugares`)       REFERENCES `Lugares`(`id`),
    FOREIGN KEY (`idIncidenteTipo`) REFERENCES `IncidenteTipo`(`id`)
);
SET FOREIGN_KEY_CHECKS=1;

INSERT INTO `Lugares` (`nombre`)
VALUE
('Centro turístico'),
('Laboratorios'),
('Restaurante'),
('Centro operativo'),
('Triceratops'),
('Dilofosaurios'),
('Velociraptors'),
('TRex'),
('Planicie de los herbívoros');

INSERT INTO `IncidenteTipo` (`nombre`)
VALUE
('Falla eléctrica'),
('Fuga de herbívoro'),
('Fuga de Velociraptors'),
('Fuga de TRex'),
('Robo de ADN'),
('Auto descompuesto'),
('Visitantes en zona no autorizada');

INSERT INTO `IncidenteSeguridad` (`idLugares`,`idIncidenteTipo`)
VALUE
(1,1),
(1,2),
(1,4),
(2,1),
(4,1),
(3,1),
(5,1);